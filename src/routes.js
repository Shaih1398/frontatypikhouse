import React from 'react';

import { asyncComponent } from '@jaredpalmer/after';

export default [
  {
    path: '/',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Home/Home'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/signin',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Auth/SignIn'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/myaccount',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Auth/MyAccount'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/category/:id',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Categories/Category'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/product/:id',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Product/ProductDetail'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/post/:id',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Component/seePost'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/confirmBooking',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Booking/ConfirmBooking'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/mes-reservations',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Booking/MyReservations'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/mention-legales',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Component/MentionLegal'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/CGU',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Component/CGU'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  },
  {
    path: '/addHouse',
    exact: true,
    component: asyncComponent({
      loader: () => import('./Booking/AddHouse'), // required
      Placeholder: () => <div>...LOADING...</div>, // this is optional, just returns null by default
    }),
  }
			/**
				<Route path="/category/:id" component={Category}/>
				<Route path="/product/:id" component={ProductDetail}/>
				<Route path="/confirmBooking" component={ConfirmBooking}/>
				<Route exact path="/verifyEmail/:token" component={confirmEmail}/>
				<ProtectedRouter path="/mes-reservations" component={MyReservations}/>
				<ProtectedRouter path="/mes-houses" component={MyHouses}/>
				<Route path="/mention-legales" component={MentionLegal}/>
       */
];
