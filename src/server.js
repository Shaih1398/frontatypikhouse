import { render } from '@jaredpalmer/after';
import express from 'express';
import routes from './routes';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);
const chunks = require(process.env.RAZZLE_CHUNKS_MANIFEST);

const server = express();


server
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', async (req, res) => {
    try {
      let html = await render({
        req,
        res,
        routes,
        assets,
        chunks,
      });

      const meta = '<meta name="viewport" content="width=device-width, initial-scale=1" id=2/> <meta name="google-site-verification" content="kOpjGtu8Bl88hbc4tgQnnIFyTbD8egmuNh3Yj2eC_FA" /> <link rel="manifest" href="/manifest.json">';
      const position = 27;
      const output = [html.slice(0, position), meta, html.slice(position)].join('');

      res.send(output);
    } catch (error) {
      console.error(error);
      res.json({ message: error.message, stack: error.stack });
    }
  });

export default server;
