import React from 'react';
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import { ATYPIC_CONFIG } from "../Auth/Config";
import axios from "axios";
import Button from "@material-ui/core/Button";
import Spinner from "../Component/Spinner";
import StarRating from "../Component/StarRating";


const BASE = ATYPIC_CONFIG.endpoint_url;


class Commentary extends React.Component {

	state = {
		rate: 0,
		comment: "",
		loading: false,
		message: null
	};

	onChangeInput = (field) => {
		return (event) => {
			this.setState({
				[field]: event.target.value,
			});
		};
	};

	onChangeStar = (newValue) => {
		this.setState({ rate: newValue })

	}

	async addComment() {
		this.setState({ loading: true });
		let { rate, comment } = this.state;
		let bookingId = this.props.eresaInfo.id;
		let houseId = this.props.eresaInfo.house.id;
		let objectComment = { comment: comment, rate: rate, bookingId: bookingId, houseId: houseId };

		try {
			const token = await localStorage.getItem("token");
			const response = await axios.post(BASE + "/account/comment", objectComment, {
				headers: {
					'x-access-token': token,
					"Content-Type": "application/json"
				}
			});
			if (response && response.status === 200) {
				this.setState({ message: "Il a bien été pris en compte", validComment: true, loading: false });
			} else {
				this.setState({ message: "Erreur. Impossible de poster votre commentaire", loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ message: "Erreur Serveur", loading: false });
		}
	}

	render() {
		return <div className="Commentary">
			{!this.state.isLoading ? !this.state.validComment && this.props.eresaInfo && this.props.eresaInfo.comment && this.props.eresaInfo.comment.bookingId && this.props.eresaInfo.comment.bookingId === this.props.eresaInfo.id ?
				<div> Vous avez déjà commenter cette réservation</div> :
				<div>
					{!this.state.validComment && <div>
						<div>
							<StarRating value={this.state.rate} onChange={(newValue) => this.onChangeStar(newValue)} />
						</div>
						<div><label htmlFor="commentSelect">Commentaire</label> <br />
							<TextareaAutosize aria-label="minimum height" onChange={this.onChangeInput("comment")} minRows={3} placeholder="Minimum 3 rows" />
							<Button variant="contained" color="primary" onClick={() => this.addComment()}>Ajouter</Button></div>
					</div>}
				</div>
				: <Spinner />}
			<div>{this.state.message}</div>
		</div>
	}
}

export default Commentary;
