import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Button from "@material-ui/core/Button";
import Commentary from "./Commentary";
import { Link } from "react-router-dom";

const BASE = ATYPIC_CONFIG.endpoint_url;
const maxDayAfterReservation = 5;

class MyReservations extends React.Component {

	async componentDidMount() {
		await this.getResa();
	}

	state = {
		infosResa: [],
		loading: false,
		visibleInputCommentary: false,
		seeReservations: false
	};


	async getResa() {
		this.setState({ loading: true });
		try {
			const token = await localStorage.getItem("token");
			const response = await axios.get(BASE + '/booking/me', {
				headers: {
					'x-access-token': token
				}
			});
			if (response && response.status === 200) {
				this.setState({ infosResa: response.data, loading: false });
			} else {
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}

	formatDate(date) {
		return new Date(date).toLocaleDateString();
	}

	checkIfCommentaryIsVisible(endDate) {
		const date1 = new Date();
		const date2 = new Date(endDate);
		const diffTime = Math.abs(date1 - date2);
		const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
		return date1 > date2 && diffDays > 0 && diffDays < maxDayAfterReservation;
	}


	checkIfResaIsActive(endDate) {
		const date1 = new Date();
		const date2 = new Date(endDate);
		return date1 < date2;
	}

	displayCommentary() {
		this.setState({ visibleInputCommentary: true });
	}

	async cancelResa(resa) {
		const token = await localStorage.getItem("token");
		const response = await axios.put(BASE + '/booking/' + resa.id, {}, {
			headers: {
				'x-access-token': token
			}
		});
		if (response && response.status === 200) {
			window.location.replace("/")
		}
	}

	render() {
		return <div className="MyReservations" style={{ margin: "10%", padding: "10px" }}>

			<div>
				<h1>Mes reservations</h1>
				{this.state.infosResa && this.state.infosResa.length > 0 && this.state.infosResa.map(resa => <div key={this.state.infosResa.id}
					style={{ borderBottom: "3px dotted black", padding: "10px" }}>
					<p>Nom : {resa.house.name}</p>
					<p>Début de séjour : {this.formatDate(resa.startDate)}</p>
					<p>Fin de séjour : {this.formatDate(resa.endDate)}</p>
					<p>Prix total du séjour : {resa.price} €</p>
					<p>Prix par nuit : {resa.house.price} €</p>
					<span>Adresse : <i>{resa.house.street}</i></span> <br />
					<span> <i>{resa.house.postalCode}, {resa.house.city} </i></span>
					<br />
					{this.checkIfCommentaryIsVisible(resa.endDate) && <Commentary eresaInfo={resa} />}
					{this.checkIfResaIsActive(resa.endDate) && resa.confirm && <Link to={"/updateBooking/" + resa.id}><h3 className="house-title" style={{ fontSize: "18px", fontWeight: "800", color: "black" }}>Modifier </h3> </Link>}
					{this.checkIfResaIsActive(resa.endDate) && resa.confirm && <Button variant="contained" color="primary" onClick={() => this.cancelResa(resa)}>Annuler ma reservation</Button>}
					{!resa.confirm && <div>Cette réservation a été annulé</div>}
				</div>)}
			</div>

		</div>
	}
}

export default MyReservations;
