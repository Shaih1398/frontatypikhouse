import React from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button";
import {ATYPIC_CONFIG} from "../Auth/Config";
import axios from "axios";
import Spinner from "../Component/Spinner";
import TextareaAutosize from "@material-ui/core/TextareaAutosize/TextareaAutosize";
import Autocomplete from "@material-ui/lab/Autocomplete";
import './AddHouse.css';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';


const BASE = ATYPIC_CONFIG.endpoint_url;

class AddHouse extends React.Component {

	async componentDidMount() {
		await this.getCategories();
		await this.getEquipments();
	}

	state = {
		name: "",
		receptionCapacity: undefined,
		price: undefined,
		nbBed: undefined,
		street: "",
		postalCode: "",
		city: "",
		country: "",
		shortDesc: "",
		longDesc: "",
		categoryId: 1,
		file: null,
		seeAddHouse: false,
		loading: false,
		selectedEquipment: [],
		dynProp:{}
	};

	handleChange = async (event) => {
		this.setState({ categoryId: event.target.value });
		await this.getCategoryBis(event.target.value);
		this.setState({ loading: false });

	};

	async getCategories() {
		this.setState({loading: true});
		try {
			const response = await axios.get(BASE + '/category');
			if (response && response.status === 200) {
				this.setState({categories: response.data, loading: false});
			} else {
				this.setState({loading: false});
			}
		} catch (error) {
			console.error(error);
			this.setState({loading: false});
		}
	}

	async getCategoryBis(id) {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/admin/category/' + id);
			if (response && response.status === 200) {
				console.log(response.data);
				this.setState({
					dynamicProperties: response.data.dynamicProperties
				})
			} else {
				this.setState({loading: false});
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({loading: false});
			this.setState({ loading: false });
		}
	}


	async getEquipments() {
		this.setState({loading: true});
		try {
			const response = await axios.get(BASE + '/equipments');
			if (response && response.status === 200) {
				console.log("get equipment : ", response.data);
				this.setState({equipments: response.data, loading: false});
			} else {
				this.setState({loading: false});
			}
		} catch (error) {
			console.error(error);
			this.setState({loading: false});
		}
	}


	handleSubmit = async () => {
		this.setState({loading: true});
        let dataArray = [];
		if (this.state.dynamicProperties && this.state.dynamicProperties.length > 0) {
			this.state.dynamicProperties.forEach(property => dataArray.push({ [property.id]: this.state[property.id] }));
		}

		try {
			if (!this.state.file && !this.state.name || !this.state.receptionCapacity || !this.state.price || !this.state.nbBed
				|| !this.state.street || !this.state.postalCode || !this.state.city || !this.state.country || !this.state.categoryId || !this.state.shortDesc || !this.state.longDesc) {
				this.setState({error: "Il faut remplir tous les champs"});
				throw new Error('Il faut remplir tous les champs');
			}

			const formData = new FormData();
			if (this.state.file) {
				for (let files of this.state.file) {
					formData.append('file', files);
				}
			}

			const house = {
				name: this.state.name,
				receptionCapacity: this.state.receptionCapacity,
				price: this.state.price,
				nbBed: this.state.nbBed,
				street: this.state.street,
				postalCode: this.state.postalCode,
				city: this.state.city,
				country: this.state.country,
				categoryId: this.state.categoryId,
				shortDesc: this.state.shortDesc,
				longDesc: this.state.longDesc,
				equipments : this.state.selectedEquipment,
				dynamicProperties : dataArray,
				status: 1,
				isBlock: 1

			};
			formData.append('house', JSON.stringify(house));

			//const newHouse = await axios.post(BASE + "/house", house);
			const token = await localStorage.getItem("token");

			const response = await axios.post(BASE + "/upload/files", formData, {
				headers: { "Content-Type": "multipart/form-data;", 'x-access-token': token }
			});
			this.setState({
				loading: false,
				confirm: "Bien Enregistré !",
				name: "",
				receptionCapacity: undefined,
				price: undefined,
				nbBed: undefined,
				street: "",
				postalCode: "",
				city: "",
				country: "",
				shortDesc: "",
				longDesc: "",
				categoryId: 1,
				file: null
			});
			this.setState({ loading: false });
			// handle success
		} catch (error) {
			// handle error
			this.setState({loading: false, error: "Erreur Serveur"});
		}
	};

	onChangeInput = (field) => {
		return (event) => {
			//console.log("on change le field", event.target.value);
			this.setState({
				[field]: event.target.value,
			});
		};
	};

	onChangeInputProperties = (field, type) => {
		let value = null;
		if (type) {
			value = !!event.target.value;
		} else {
			value = event.target.value
		}
		return (event) => {
			//console.log("on change le field", event.target.value);
			this.setState({ ...this.state.dynProp, [field]: value });
		};
	};


	onTagsChange = (event, values) => {
		this.setState({
			selectedEquipment: values
		}, () => {
			// This will output an array of objects
			// given by Autocompelte options property.
			console.log(this.state.selectedEquipment);
		});
	};

	render() {
		return <div  className="AddHouse" style={{ width :"100%"}}>
		<div style={{marginLeft : "2rem"}}>
					<h1 align="center" style={{ color: "#ab3535", fontStyle: "italic", marginTop: "10%" }}>Ajouter un logement</h1>
			        <div>
					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="name"
							   label="Nom"
							   type="text"
							   className={"name"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.name}
							   onChange={this.onChangeInput("name")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="receptionCapacity"
							   label="Capacité"
							   type="number"
							   className={"receptionCapacity"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.receptionCapacity}
							   onChange={this.onChangeInput("receptionCapacity")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="price"
							   label="Prix"
							   type="number"
							   className={"price"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.price}
							   onChange={this.onChangeInput("price")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="nbBed"
							   label="Nombre de lit"
							   type="number"
							   className={"nbBed"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.nbBed}
							   onChange={this.onChangeInput("nbBed")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="street"
							   label="Rue"
							   type="text"
							   className={"street"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.street}
							   onChange={this.onChangeInput("street")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="postalCode"
							   label="Code postal"
							   type="text"
							   className={"postalCode"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.postalCode}
							   onChange={this.onChangeInput("postalCode")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="city"
							   label="city"
							   type="text"
							   className={"city"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.city}
							   onChange={this.onChangeInput("city")}/>

					<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
							   id="country"
							   label="country"
							   type="text"
							   className={"country"}
							   InputLabelProps={{shrink: true}}
							   value={this.state.country}
							   onChange={this.onChangeInput("country")}/>
			<Select
				native
				value={this.state.categoryId}
				onChange={(e) => this.handleChange(e)}
				inputProps={{
					name: 'Catégorie',
					id: 'Catégorie',
				}}
				style ={{marginTop : "16px", marginLeft : "10px"}}
			>
				{this.state.categories && this.state.categories.map(category =>
					<option value={category.id}>{category.name}</option>)
				}
			</Select>
					<div>



						{this.state.dynamicProperties && this.state.dynamicProperties.map(property =>
							<div>
								<p>{property.name}</p>
								{
									property.type === "boolean" ? <Grid item xs={12}>
											<FormControlLabel
												control={<Checkbox color="primary"
																   onChange={this.onChangeInputProperties(property.id)} />}
												label={property.name}
											/>
										</Grid> :
										<TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
												   id={property.name}
												   label={property.name}
												   type={property.type}
												   className={property.name}
												   InputLabelProps={{ shrink: true }}
												   value={this.state[property.name]}
												   onChange={this.onChangeInputProperties(property.id)} />}
							</div>
						)}



						<br />



					</div>
					<br/>
					{this.state.equipments && this.state.equipments.length > 0 && <Autocomplete
						multiple
						id="tags-standard"
						options={this.state.equipments}
						getOptionLabel={(option) => option.name}
						onChange={this.onTagsChange}
						renderInput={(params) => (
							<TextField style={{marginLeft : "8px", width : "91%"}}
								{...params}
								variant="standard"
								label="Equipements"
								placeholder="Equipements"
							/>
						)}
					/>}

					<div>
						<TextField style={{marginLeft: "10px", marginRigth: "10px", width :"91%", marginTop : "5%"}}
								   label="Description courte"
								   type="text"
								   className={"name"}
								   InputLabelProps={{shrink: true}}
								   onChange={this.onChangeInput("shortDesc")}/>
				 </div>

            <div style={{width :"100%", marginTop : "5%"}}>
			<TextField
				id="outlined-multiline-static"
				label="Longue description"
				multiline
				rows={10}
				variant="outlined"
				onChange={this.onChangeInput("longDesc")}
				placeholder="Longue description"
				style ={{width : "91%"}}
			/>
			</div>



				<div className="upload-btn-wrapper">
				<button  className="btn">Ajouter vos photo</button>
					<input  name="myfile" type="file" multiple onChange={event => this.setState({file: event.target.files})}/>
				</div>




                    <div align="center" style ={{marginTop : "3%"}}>
					<Button variant="contained" color="primary" onClick={() => this.handleSubmit()}>Enregistrer ma house</Button>
						{!this.state.confirm && this.state.error && <p style={{ color: "#FF0000" }}> {this.state.error}</p>}
					<p style={{color: "#538656"}}> {this.state.confirm}</p>
					</div>
				</div>
		</div>
		</div>
	}
}

export default AddHouse;
