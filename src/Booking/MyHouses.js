import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Button from "@material-ui/core/Button";
import "../css/category.css";
import { Link } from "react-router-dom";
import RoomIcon from "@material-ui/icons/Room";
import StarRating from "../Component/StarRating";
import KingBedIcon from "@material-ui/icons/KingBed";
import BathtubIcon from "@material-ui/icons/Bathtub";
import {useStyles} from "../Home/styles";
import Container from "@material-ui/core/Container";
import FooterCusto from "../Component/FooterCusto";

const BASE = ATYPIC_CONFIG.endpoint_url;

class MyHouses extends React.Component {


	async componentDidMount() {
		await this.getMyHouses();
	}

	state = {
		infosHouses: [],
		loading: false,
		seeHouse: false,
		removeHouse: false
	};

	async getMyHouses() {
		this.setState({ loading: true });
		try {
			const token = await localStorage.getItem("token");
			const response = await axios.get(BASE + '/house/me', {
				headers: {
					'x-access-token': token
				}
			});
			if (response && response.status === 200) {
				this.setState({ infosHouses: response.data, loading: false });
			} else {
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}

	async cancelHouse(house) {
		const token = await localStorage.getItem("token");
		const response = await axios.put(BASE + '/cancel/house/' + house.id, {}, {
			headers: {
				'x-access-token': token
			}
		});
		if (response && response.status === 200) {
			this.setState({ removeHouse: true, messageRemove: "La house a bien été supprimé" });
		}
	}


	render() {
		return <>
			<main style={{ backgroundColor: "#F7F7F7", marginTop :"9%" }}>
				<Container style={{ marginTop: "5%", maxWidth: "1000px" }} className={useStyles.cardGrid} >
					<div >
						<h1 style={{color : "#ab3535"}} align="center">Mes logements</h1>
						{this.state.removeHouse && <p style={{ "color": "red" }}>{this.state.messageRemove}</p>}
						<div>
				<div className= "c-flex-wrap-category">

				{this.state.infosHouses && this.state.infosHouses.length > 0 && this.state.infosHouses.map(resa => <div key={resa.id} className="c-card-cat" style={{ width: "33%", marginBottom: "3%", backgroundColor: "white", borderRadius: "5px", position: "relative", overflow: "hidden" }}>
						<div style={{ height: "175px", overflow: "hidden", clear: "both" }}>
							<img style={{ width: "100%", height: "100%", borderTopLeftRadius: "3%", borderTopRightRadius: "3%" }} src={"https://imageatypic.s3.us-east-1.amazonaws.com/" + resa.houseImgs[0].img} className={"imgCategory"} alt={resa.houseImgs[0].img} />
						</div>
						<div style={{ minHeight: "120px" }}>
							<div style={{
								fontSize: "0.8rem", minHeight: "50px",
								color: "#9fa7a7", fontWeight: "bold", border: "1px solid #E4E4E4", padding: "0px 20px 20px"
							}}>
								<div style={{ minHeight: "65px" }}>
									<Link to={"/seeReservationHouse/" + resa.id}><h3 className="house-title" style={{ fontSize: "18px", fontWeight: "800", color: "black" }}>{resa
										.name} </h3> </Link> </div>
								<div>
									<div style={{ display: "flex" }}>
										<div><RoomIcon style={{ width: "0.6em" }} /></div>
										<div style={{ marginTop: "0px" }}><span style={{ fontSize: "12px", color: "#aeb4b6" }}>{resa.city}</span></div>
									</div>

								</div>

							</div>
						</div>
						<div style={{ padding: "20px 20px 20px", border: "1px solid #E4E4E4", borderTop: "none" }}>
							<div style={{ justifyContent: "space-around" }} className="c-flex-wrap-category">
								<div className="c-flex" >
									<KingBedIcon style={{ color: "black" }} ></KingBedIcon>
								</div>
								<div>
									<span style={{ fontSize: "13px", color: "black" }}> {resa.nbBed} Chambre(s)</span>
								</div>
								<div className="c-flex" >
									<BathtubIcon style={{ color: "black" }}></BathtubIcon>
								</div>
								<div>
									<span style={{ fontSize: "13px", color: "black" }}> {resa.nbBed} Salle de bain(s)</span>
								</div>
							</div>
							<div style={{ display: "flex" }}>
								<div style={{ marginRight: "31%", marginTop: "20px" }}><span style={{ fontSize: "20px", color: "black", fontWeight: "bold" }}>{resa.price}€/nuit </span></div>
								<div style={{ marginTop: "20px" }}>
									<Link to={"/seeReservationHouse/" + resa.id}><h3 className="house-title" style={{ fontSize: "18px", fontWeight: "800", color: "black" }}>Voir les reservations </h3> </Link>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		</div>
		</div>
				</Container>
			</main>
			{this.state.loading ? "" : <FooterCusto />}
		</>

	}
}

export default MyHouses;
