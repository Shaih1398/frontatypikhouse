import React from 'react';
import Button from "@material-ui/core/Button";

class MyInformations extends React.Component {

	state = {
		seeInformations: false
	};

	render() {
		return <div className="MyInformations" style={{margin: "25px", padding: "10px"}}>
			<div>
				<p>Content de vous revoir {this.props.user.firstName}</p>
				<p>Prenom : {this.props.user.firstName}</p>
				<p>Nom de famille : {this.props.user.lastName}</p>
				<p>Rue : {this.props.user.street}</p>
				<p>Code postal : {this.props.user.postalCode}</p>
				<p>Ville : {this.props.user.city}</p>
				<p>Pays : {this.props.user.country}</p>
				<p>Email : {this.props.user.email}</p>
			</div>
		</div>
	}
}

export default MyInformations;
