import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import Spinner from "../Component/Spinner";
import { CheckoutComponent } from "../Product/CheckoutComponent";
import { loadStripe } from "@stripe/stripe-js";
import { Elements, ElementsConsumer } from "@stripe/react-stripe-js";

const BASE = ATYPIC_CONFIG.endpoint_url;
const promise = loadStripe(ATYPIC_CONFIG.publicKeyStripesTest);

class UpdateBooking extends React.Component {

    async componentDidMount() {
        await this.getResa();
    }

    state = {
        infosResa: [],
        loading: false,
        visibleInputCommentary: false,
        seeReservations: false,
        open: false,
        picker1: "",
        picker2: "",
        validPicker: false,
        textValid: "",
        numberOfNight: 0,
        totalPrice: 0,
        isVisiblePay: false,
    };

    async getResa() {
        this.setState({ loading: true });
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.get(BASE + '/booking/' + this.props.match.params.id, {
                headers: {
                    'x-access-token': token
                }
            });
            console.log("response : ", response);
            if (response && response.status === 200) {
                this.setState({ infosResa: response.data, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    onChangeInput = (field) => {
        return (event) => {
            this.setState({
                [field]: event.target.value,
            });
        };
    };

    getDateFormattingForField() {
        return new Date().toISOString().split('T')[0]
    }

    formatDate(date) {
        return new Date(date).toLocaleDateString();
    }

    checkIfResaIsActive(endDate) {
        const date1 = new Date();
        const date2 = new Date(endDate);
        return date1 < date2;
    }

    getDateFormattingForField() {
        return new Date().toISOString().split('T')[0]
    }

    async getAvailability(beginDate, endDate, houseId) {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/booking/available/' + houseId + '/' + beginDate + '/' + endDate);
            if (response && response.status === 200) {
                this.setState({ loading: false });
                return response.data;
            } else {
                this.setState({ loading: false });
                return false;
            }
        } catch (error) {

            this.setState({ loading: false });
        }
    }

    getPopIn(houseId) {
        return <div className={"c-booking"}>
            <h3 style={{ color: "#ab3535" }}>Vérifier les disponibilités</h3>
            <TextField style={{ width: "100%", }}
                id="picker1"
                label="Arrivée"
                type="date"
                className={"textField"}
                InputLabelProps={{ shrink: true }}
                value={this.state.picker1}
                InputProps={{ inputProps: { min: this.getDateFormattingForField() } }}
                onChange={this.onChangeInput("picker1")} />
            <TextField style={{ width: "100%", marginTop: "20px" }}
                id="picker2"
                label="Départ"
                type="date"
                className={"textField"}
                InputLabelProps={{ shrink: true }}
                value={this.state.picker2}
                disabled={!this.state.picker1}
                InputProps={{ inputProps: { min: this.state.picker1 ? this.state.picker1 : null } }}
                onChange={this.onChangeInput("picker2")} />

            <Button style={{ marginTop: "20px", width: "50%" }} variant="contained" color="primary" onClick={() => this.handleClose(houseId)}>Voir les disponibilités</Button>
            {!this.state.validPicker && this.state.picker1 && this.state.picker2 && this.state.textValid}
        </div>
    }


    getNumberOfNight(beginDate, endDate) {
        let date1 = new Date(beginDate);
        let date2 = new Date(endDate);

        // To calculate the time difference of two dates
        let differenceInTimes = date2.getTime() - date1.getTime();

        // To calculate the no. of days between two dates
        return differenceInTimes / (1000 * 3600 * 24);
    }

    async handleClose(houseId) {
        let available;

        if (this.state.picker1 && this.state.picker2) {
            available = await this.getAvailability(this.state.picker1, this.state.picker2, houseId);
            if (available && available.canBook) {
                const numberOfNight = this.getNumberOfNight(this.state.picker1, this.state.picker2);
                if (numberOfNight > 0) {
                    const totalPrice = this.getTotalPrice(this.state.infosResa[0].house.price, numberOfNight);
                    this.setState({ validPicker: true, textValid: "Ces dates sont disponibles", numberOfNight: numberOfNight, totalPrice: totalPrice });
                } else {
                    this.setState({ validPicker: false, textValid: "Ces dates sont indisponibles", numberOfNight: 0, totalPrice: 0 });
                }
            } else {
                this.setState({ validPicker: false, textValid: "Ces dates sont indisponibles", numberOfNight: 0, totalPrice: 0 });
            }
        } else {
            this.setState({ validPicker: false, textValid: "Vous avez mal selectionnés vos dates", numberOfNight: 0, totalPrice: 0 });
        }

        this.setState({
            open: false
        })
    }

    getTotalPrice(priceByNight, numberOfNight) {
        return priceByNight * numberOfNight;
    }

    render() {
        return <div className="MyReservations" style={{ margin: "100px", padding: "10px" }}>
            <h1>Modification de ma reservation</h1>
            {this.state.infosResa && this.state.infosResa.length > 0 ? <div>
                <h1>Reservation No {this.state.infosResa[0].id}</h1>
                <p>Lieu de résidence : {this.state.infosResa[0].house.name}</p>
                <p>Date de début : {this.formatDate(this.state.infosResa[0].startDate)}</p>
                <p>Date de fin : {this.formatDate(this.state.infosResa[0].endDate)}</p>
                <p>Prix par nuit : {this.state.infosResa[0].price}</p>
            </div> : <p>Aucun information</p>}
            {this.getPopIn()}
            <p>{this.state.numberOfNight ? this.state.numberOfNight : null}</p>
            <p>{this.state.totalPrice ? this.state.totalPrice : null}</p>
            {this.state.isVisiblePay && <Elements stripe={promise}>
                <ElementsConsumer>{({ elements, stripe }) => (
                    <CheckoutComponent elements={elements} stripe={stripe} amount={this.state.totalPrice} idHouse={this.state.infosResa[0].house.id} history={this.props.history}
                        begin={this.state.picker1} end={this.state.picker2} cancelResaId={this.state.infosResa[0].id}/>
                )}
                </ElementsConsumer>
            </Elements>}
            <Button variant="contained" color="primary" onClick={() => this.setState({isVisiblePay : true})}>Valider mes modifications</Button>
        </div>
    }
}

export default UpdateBooking;