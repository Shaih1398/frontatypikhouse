import React from 'react';

class ConfirmBooking extends React.Component {

	render() {
		return <div className="ConfirmBooking" style={{ marginTop: "100px" }}>
			Confirmation de votre reservation ! <br />
			Montant : {this.props.location.state.amount} <br />
			Début du séjour : {this.props.location.state.begin}<br />
			Fin du séjour : {this.props.location.state.end}<br />
		</div>
	}
}

export default ConfirmBooking;
