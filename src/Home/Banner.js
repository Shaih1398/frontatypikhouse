import React from 'react';
import Carousel from 'react-material-ui-carousel'
import {Paper, Button} from '@material-ui/core'

export function Banner(props) {
	const items = [
		{
			name: "Random Name #1",
			description: "Probably the most random thing you have ever seen!",
			img: "https://source.unsplash.com/random"
		},
		{
			name: "Random Name #2",
			description: "Hello World!",
			img: "https://source.unsplash.com/random"
		}
	];

	return (
		<div>
			<Carousel interval={4000} animation={"fade"}>
				{
					items.map((item, i) => <Item key={i} item={item}/>)
				}
			</Carousel>
		</div>
	)
}

function Item(props) {
	return (
		<Paper>
			<img src={props.item.img} style={{"width": "100%", "height": "200px"}} alt={props.item.name}/>
			<h2>{props.item.name}</h2>
			<p>{props.item.description}</p>
			<Button className="CheckButton">
				Check it out!
			</Button>
		</Paper>
	)
}
