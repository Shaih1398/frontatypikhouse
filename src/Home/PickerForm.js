import React from 'react';
import TextField from '@material-ui/core/TextField';
import "./home.css"
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';

class PickerForm extends React.Component {

	state = {
		town: "",
		picker1: "",
		picker2: "",
		nbVoyageurs: ""
	};

	onChangeInput = (field) => {
		return (event) => {
			this.setState({
				[field]: event.target.value,
			});
		};
	};

	callApiSearch() {
		console.log(this.state);
	}


	render() {
		//const classes = useStyles;
		return <div>
			<form className={"container"}  noValidate>
				<TextField style={{marginLeft: "10px",   marginRigth: "10px"}}
						   id="town"
						   label="Ville"
						   type="string"
						   className={"textField"}
						   InputLabelProps={{
							   shrink: true,
						   }}
						   value={this.state.town}
						   onChange={this.onChangeInput("town")}
				/>
				<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
						   id="picker1"
						   label="Arrivée"
						   type="date"
						   className={"textField"}
						   InputLabelProps={{
							   shrink: true,
						   }}
						   value={this.state.picker1}
						   onChange={this.onChangeInput("picker1")}
				/>
				<TextField style={{marginLeft: "10px", marginRigth: "10px"}}
						   id="picker2"
						   label="Départ"
						   type="date"
						   className={"textField"}
						   InputLabelProps={{
							   shrink: true,
						   }}
						   value={this.state.picker2}
						   onChange={this.onChangeInput("picker2")}
				/>
				<TextField style={{marginLeft: "10px", marginRigth: "10px", width : "20%"}}
						   id="nbVoyageurs"
						   label="Nb de voyageurs"
						   type="number"
						   className={"textField"}
						   InputLabelProps={{
							   shrink: true,
						   }}
						   value={this.state.nbVoyageurs}
						   onChange={this.onChangeInput("nbVoyageurs")}
				/>

				<SearchRoundedIcon style={{marginTop : "15px", marginLeft : "2rem", cursor : "pointer"}} onClick={() => this.callApiSearch()}/>
			</form>
		</div>
	}
}

export default PickerForm;
