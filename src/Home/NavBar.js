import {
	AppBar,
	Toolbar,
	Typography,
	makeStyles,
	Button,
	IconButton,
	Drawer,
	Link,
	MenuItem,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import React, { useState, useEffect } from "react";
import { Link as RouterLink } from "react-router-dom";
import { logoutUser } from "../Middleware/middleware";
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import Menu from '@material-ui/core/Menu';
import { Link as Redirection } from "react-router-dom";
import './NavBar.css';

const headersData = [
	{
		label: "Accueil",
		href: "/",
		isConnected: true,
		logout: false
	},

	{
		label: "Profil",
		href: "",
		isConnected: typeof window !== 'undefined' && !!localStorage.getItem("token"),
		logout: true,
		handle: true
	},

	{
		label: "Se déconnecter",
		href: "/",
		isConnected: typeof window !== 'undefined' && !!localStorage.getItem("token"),
		logout: true
	},
	{
		label: "Se connecter",
		href: "/signin",
		isConnected: typeof window !== 'undefined' && !localStorage.getItem("token"),
		logout: false
	},
	{
		label: "Admin",
		href: "/admin",
		isConnected: typeof window !== 'undefined' && !!localStorage.getItem("status"),
		logout: false,
	}


];

const useStyles = makeStyles(() => ({
	header: {
		backgroundColor: "white",

		borderBottom: "3px solid #61B345",
		"@media (max-width: 900px)": {
			paddingLeft: 0,
		},
	},
	logo: {
		fontFamily: "Work Sans, sans-serif",

		color: "black",
		textAlign: "left",
	},
	menuButton: {
		fontFamily: "Open Sans, sans-serif",
		color: "#413122",
		fontWeight: 700,
		size: "18px",
		marginLeft: "38px",
		marginRight: "38px",

	},
	toolbar: {
		display: "flex",
		justifyContent: "space-between",
	},
	drawerContainer: {
		padding: "20px 30px",
	},

}));

export default function NavBar() {
	const { header, logo, menuButton, toolbar, drawerContainer } = useStyles();
	const [state, setState] = useState({
		mobileView: false,
		drawerOpen: false,
	});
	const [anchorEl, setAnchorEl] = React.useState(null);

	const { mobileView, drawerOpen } = state;

	useEffect(() => {
		const setResponsiveness = () => {
			return window.innerWidth < 900
				? setState((prevState) => ({ ...prevState, mobileView: true }))
				: setState((prevState) => ({ ...prevState, mobileView: false }));
		};

		setResponsiveness();

		window.addEventListener("resize", () => setResponsiveness());
	}, []);

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = (link) => {
		setAnchorEl(null);
	};

	const displayDesktop = () => {
		return (
			<Toolbar className={toolbar}>
				{logoHouse}
				<div>{getMenuButtons()}</div>
				<Menu
					style={{marginRight :"38px", marginLeft :"38px"}}
					id="simple-menu"
					anchorEl={anchorEl}
					keepMounted
					open={Boolean(anchorEl)}
					onClose={handleClose}>
					<Redirection to={"/myaccount"}><MenuItem style={{color : "black"}} onClick={handleClose}>Profile</MenuItem></Redirection>
					<Redirection to={"/mes-reservations"}><MenuItem style={{color : "black"}} onClick={handleClose}>Mes reservations</MenuItem></Redirection>
					<Redirection to={"/mes-houses"}><MenuItem style={{color : "black"}} onClick={handleClose}>Mes Houses</MenuItem></Redirection>
					<Redirection to={"/addHouse"}><MenuItem style={{color : "black"}} onClick={handleClose}>Ajouter une House</MenuItem></Redirection>
				</Menu>
			</Toolbar>
		);
	};

	const displayMobile = () => {
		const handleDrawerOpen = () =>
			setState((prevState) => ({ ...prevState, drawerOpen: true }));
		const handleDrawerClose = () =>
			setState((prevState) => ({ ...prevState, drawerOpen: false }));

		return (
			<Toolbar>
				<IconButton
					{...{
						edge: "start",
						color: "black",
						"aria-label": "menu",
						"aria-haspopup": "true",
						onClick: handleDrawerOpen,
					}}
				>
					<MenuIcon />
				</IconButton>

				<Drawer
					{...{
						anchor: "left",
						open: drawerOpen,
						onClose: handleDrawerClose,
					}}
				>
					<div className={drawerContainer}>{getDrawerChoices()}
						<Menu
							style={{ marginRight: "38px", marginLeft: "38px" }}
							id="simple-menu"
							anchorEl={anchorEl}
							keepMounted
							open={Boolean(anchorEl)}
							onClose={handleClose}>
							<Redirection to={"/myaccount"}><MenuItem style={{ color: "black" }} onClick={handleClose}>Profile</MenuItem></Redirection>
							<Redirection to={"/mes-reservations"}><MenuItem style={{ color: "black" }} onClick={handleClose}>Mes reservations</MenuItem></Redirection>
							<Redirection to={"/mes-houses"}><MenuItem style={{ color: "black" }} onClick={handleClose}>Mes Houses</MenuItem></Redirection>
							<Redirection to={"/addHouse"}><MenuItem style={{ color: "black" }} onClick={handleClose}>Ajouter une House</MenuItem></Redirection>
						</Menu>

					</div>

				</Drawer>

				<div>{logoHouse}</div>
			</Toolbar>
		);
	};

	const getDrawerChoices = () => {
		return headersData.map(({ label, href, isConnected, logout, handle }) => {
			if (isConnected) {
				if (handle) {
					return (
						<Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
							<b>Profile</b>
						</Button>
					)
				}
				return (
					<Link
						{...{
							component: RouterLink,
							to: href,
							color: "inherit",
							style: { textDecoration: "none" },
							key: label,
						}}
					>
						<MenuItem>{label}</MenuItem>
					</Link>
				);
			}

		});
	};

	const logoHouse = (
		<Typography className={logo}>
			<img style={{ width: "16%" }} src={logoAtypik} alt="Logo AtypikHouse" />
		</Typography>
	);

	const getMenuButtons = () => {
		// eslint-disable-next-line array-callback-return
		return headersData.map(({ label, href, isConnected, logout, handle }) => {
			if (isConnected) {
				if(handle){
					return (
						<Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
							<b>Profile</b>
						</Button>
					)
				}
				return (
					<span>
						<Button onClick={() => logout ? logoutUser() : null}
							{...{
								key: label,
								color: "inherit",
								to: href,
								component: RouterLink,
								className: menuButton,
							}}>
							{label}
						</Button>
					</span>
				);
			}
		});
	};

	return (
		<header>
			<AppBar className={header}>
				{mobileView ? displayMobile() : displayDesktop()}
			</AppBar>
		</header>
	);
}