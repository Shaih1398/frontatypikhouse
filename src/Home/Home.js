import React from 'react'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import PickerForm from "./PickerForm";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { Link } from "react-router-dom";
import { useStyles } from "./styles";
import { ATYPIC_CONFIG } from "../Auth/Config";
import PaymentIcon from '@material-ui/icons/Payment';
import PersonIcon from '@material-ui/icons/Person';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';
import slider1 from '../Assets/slider1.jpeg';
import FooterCusto from "../Component/FooterCusto";
import WokeUp from "../Assets/MOKE-UP.png"
import ImgPK from "../Assets/imgPk.jpg";
import {Helmet} from "react-helmet";

const BASE = ATYPIC_CONFIG.endpoint_url;
const arrayTest = [1, 2];

class Home extends React.Component {

	async componentDidMount() {
		await this.getCategories();
		await this.getPosts();

	}

	state = {
		categories: [],
		loading: false,
		posts: [],
	};

	async getCategories() {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/category');
			if (response && response.status === 200) {
				this.setState({ categories: response.data, loading: false });
			} else {
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}

	async getPosts() {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/admin/posts');
			if (response && response.status === 200) {
				this.setState({ posts: response.data, loading: false });
			} else {
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}




	Copyright() {
		return (
			<Typography style={{ color: "white" }}>
				{'© 2021 AtypikHouse '}

				{new Date().getFullYear()}
				{'.'}
			</Typography>
		);
	}

	render() {
		return (
			<React.Fragment>
				<Helmet>
					<meta charSet="utf-8" />
					<title>Accueil</title>
					<meta name="description" content="Désormais louez une maison atypique  n'a jamais été aussi facile,
					avec des moyens de paiements sécurisée et une application assez fluide et clair" />
				</Helmet>

				<section className="banner d-flex align-items-center" style={{
					backgroundImage: `url(${slider1})`
				}}>
					<div style={{ padding: "0 15px" }}>
						<div className="row">
							<div className="col-lg-12">
								<div align="center">
									<h1 className="h1-banner">Réservez des lieux de séjour et <br />
										des activités uniques
									</h1>
								</div>
							</div>
							<div style={{ width: "100%", display: "flex", justifyContent: "center" }}>
								<div align="center" className="searchBar" style={{ backgroundColor: "white" }}><PickerForm></PickerForm></div>
							</div>
						</div>
					</div>
				</section>

				<CssBaseline />

				<main>
					{/* Hero unit */}
					<div style={{ display: "flex", justifyContent: "center", width: "100%" }}>

					</div>
					<Container className={useStyles.cardGrid} maxWidth="lg">
						{/* End hero unit */}
						<div align="center">
							<h1 style={{ color: "#ab3535", fontStyle: "italic", marginTop: "6%" }}>NOS MAISONS ATYPIQUES</h1>
						</div>

							<div style={{ marginTop: "75px"}} className="c-flex-wrap-category">
								{this.state.categories && this.state.categories.length > 0 &&
									this.state.categories.map((card, index) => (
										<div className="card-cat" style={{ marginRight: "3%" }}>
											<div>
												{card.categoryImgs && card.categoryImgs.length > 0 && card.categoryImgs[0] && card.categoryImgs[0].img ? <img style={{ width: "100%", maxHeight : "179px", height: "100%", borderRadius: "4%", boxShadow: "2px 0 8px black" }} src={"https://imageatypic.s3.us-east-1.amazonaws.com/" + card.categoryImgs[0].img}
																																							  alt={"homepageImg" + index} />
													: <img style={{ width: "100%", height: "100%", borderRadius: "4%", boxShadow: "2px 0 8px black" }} src={"/img/img1.jpeg"}
														   alt={"homepageImg" + index} />}</div>

											<div style={{ marginTop: "40px" }} align={"center"}><Button style={{ backgroundColor: "#61B345", borderRadius: "20px", width: "12rem", marginBottom: "1rem" }}
												size="small" color="primary">
												<Link to={"/category/" + card.id}><span style={{ fontWeight: "bold" }}>{card.name} </span></Link>
											</Button></div>
										</div>
									))}

						</div>

					</Container>

					<Container className={useStyles.cardGrid} maxWidth="lg">
						<div style={{ display: "flex", justifyContent: "center" }}>
							<h1 style={{ color: "#ab3535", fontStyle: "italic", marginTop: "6%" }}>POURQUOI CHOISIR ATYPIKHOUSE ?</h1>
						</div>
						<div className="c-flex-no-wrap" style={{ marginTop: "70px" }}>


							<div className="c-flex-no-wrap"  >

								<div className="blockImgPk" style={{ width: "70%" }}>
									<img style={{ width: "100%", height: "100%" }} src={ImgPK} alt = "Jolies cabanes au bord de l'eau"/>
								</div>

								<div style={{ color: "", "lineHeight": "1.42857143", fontSize: "15px", width: "70%" }}>
									<p style={{ textAlign: "justify" }}>AtypikHouse sélectionne pour vous des hébergements insolites partout en France !

										Découvrez des endroits atypiques au cœur de nos belles régions françaises : dans des cadres enchanteurs, AtypikHouse vous garantit un séjour unique hors des
										sentiers battus ! Lov’Nid, igloos, tipis, tanières de Hobbit, et bien plus encore ! Défiez le vertige dans une cabane à 12 mètres de hauteur, laissez-vous flotter
										sur une cabane au milieu d’un lac, observez les étoiles depuis votre lit dans une bulle ou expérimentez la vie de bohème dans une roulotte… </p>

									<p style={{ textAlign: "justify" }}> Grâce à nous, vous vivrez une véritable expérience hors du commun, loin des standards des hôtels classiques... Feuilles, animaux de
										la forêt et petits insectes seront sans doute de la partie ! </p>

									<p style={{ textAlign: "justify" }}>Réservez instantanément votre coup de cœur directement en ligne depuis smartphone, tablette ou ordinateur. A moins de 2 heures de chez
										vous ou à l’autre bout de la France, vous êtes sûrs de trouver l’hébergement de vos rêves grâce à notre moteur de recherche intuitif et son système de
										géolocalisation. Sélectionnez une date, un type d'hébergement, une thématique et... </p>

									<p style={{ textAlign: "justify" }}> Offrez-vous la magie d’une nuit insolite ! </p>

									<p style={{ textAlign: "justify" }}>	Pourquoi choisir AtypikHouse ? On vous en dit plus ici : </p>
								</div>

							</div>

						</div>
						<div>


							<div align="center">
								<h1 style={{ color: "#ab3535", fontStyle: "italic", marginTop: "6%" }}>NOS ENGAGEMENTS </h1>
							</div >
							<div className="c-flex-no-wrap" style={{ justifyContent: "space-Between", marginTop: "75px" }} >
								<div align="center" >
									<PaymentIcon className="c-brown" style={{ fontSize: "100px", marginRight: "4%", marginLeft: "7%", marginTop: "2%" }} />
									<div align="center" style={{ width: "50%" }}>
										<p className="c-brown" ><strong>Paiement sécurisé </strong> <br /> <br />La gestion de nos paiements en ligne sont Stripe grâce à leurs services 100% Sécurisés</p> </div>
								</div>
								<div align="center">
									<PersonIcon className="c-brown" style={{ fontSize: "100px", marginRight: "4%", marginLeft: "7%", marginTop: "2%" }} />
									<div align="center" style={{ width: "50%" }}>
										<p className="c-brown"><strong>Paiement sécurisé </strong> <br /> <br />La gestion de nos paiements en ligne sont Stripe grâce à leurs services 100% Sécurisés</p>
									</div>
								</div>
								<div align="center">
									<LocalOfferIcon className="c-brown" style={{ fontSize: "100px", marginRight: "4%", marginLeft: "7%", marginTop: "2%" }} />
									<div align="center" style={{ width: "50%" }}>
										<p className="c-brown"><strong>Paiement sécurisé </strong> <br /> <br />La gestion de nos paiements en ligne sont Stripe grâce à leurs services 100% Sécurisés</p>
									</div>
								</div>
								<div align="center">
									<CancelPresentationIcon className="c-brown" style={{ fontSize: "100px", marginRight: "4%", marginLeft: "7%", marginTop: "2%" }} />
									<div align="center" style={{ width: "50%" }}>
										<p className="c-brown"><strong>Paiement sécurisé </strong> <br /> <br />La gestion de nos paiements en ligne sont Stripe grâce à leurs services 100% Sécurisés</p>
									</div>
								</div>
							</div>



						</div>
					</Container>

					<Container className={useStyles.cardGrid} maxWidth="lg">
						<div style={{ display: "flex", justifyContent: "center" }}>
							<h1 style={{ color: "#ab3535", fontStyle: "italic", marginTop: "6%" }}>APPLICATION MOBILE</h1>
						</div>
						<div align="center" className="woke" >
							<div className="textWoke" style={{ textAlign: "justify" }} >
								<p style={{fontSize :"17px"}} >
									Des voyages inoubliables vous attendent sur AtypikHouse.
									Réservez le séjour idéal et vivez de nouvelles expériences ! Choisissez parmi une multitude d’annonces , vous avez l’embarras du choix.
									Optez pour une cabane dans les arbres ou un tipi pour un week-end fantaisiste, que vous soyez seul, en famille ou en couple !
									Trouvez des activités et des excursions uniques animées par des experts locaux, il y en a pour tous les âges et tous les goûts.
									Les séjours avec conditions d’annulation flexibles peuvent vous rassurer si vous souhaitez planifier un voyage qui pourrait subir des changements.
									Effectuez vos recherches en toute tranquillité via notre <b>application mobile ! </b>
								</p>
							</div>
							<div>
								<img style={{ width: "100%" }} src={WokeUp} alt="app mobile" />
							</div>
						</div>
					</Container>


					<Container className={useStyles.cardGrid} maxWidth="md"  >
						<div align="center">
							<h1 style={{ color: "#ab3535", fontStyle: "italic", marginTop: "6%" }}>CONSEILS & ACTUALITES</h1>
						</div>

						<Grid container spacing={4} style={{ marginBottom: "6%" }}>
							{this.state.posts && this.state.posts.length > 0 &&
							this.state.posts.map((card, index) => (
									<Grid item key={card} xs={12} sm={6} md={4} style={{ marginTop: "75px" }}>
										<Card className={useStyles.card}>
											<CardMedia
												className={useStyles.cardMedia}
												image="https://source.unsplash.com/random"
												title="Image title"
											/>
											{/*<img src={"https://source.unsplash.com/random"} alt={"random"}/>*/}
											<CardContent className={useStyles.cardContent}>
												<Typography gutterBottom variant="h5" component="h2">
													<div>
														<img style={{width : "100%"}} src={"https://imageatypic.s3.us-east-1.amazonaws.com/" + card.image} className={"imgCategory"} alt={card.image} />
													</div>
												</Typography>
												<Typography className="titleTipo" gutterBottom variant="h5" component="h2">
													<Link  to={"/post/" + card.id}><span style={{ fontWeight: "bold", color : "#61b345" }}>{card.title} </span></Link>
												</Typography>
												<Typography>
													{card.content.substring(0, 50)}
												</Typography>
											</CardContent>

										</Card>
									</Grid>
								))}
						</Grid>
					</Container>

				</main>
				{/* Footer */}
				<FooterCusto></FooterCusto>

				{/* End footer */}
			</React.Fragment>
		);
	}
}

export default Home;
