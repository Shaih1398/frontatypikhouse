import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from "react-router-dom";
import AppRouting from "./Router/AppRouting";
//import history from './history';
import { hydrate } from 'react-dom';
import { ensureReady, After } from '@jaredpalmer/after';
import routes from './routes';
import NavBar from "./Home/NavBar";
import * as OfflinePluginRuntime from "offline-plugin/runtime";
OfflinePluginRuntime.install();


ensureReady(routes).then(data =>
	hydrate(
		<BrowserRouter>
			<NavBar />
			<AppRouting />
		</BrowserRouter>,
		document.getElementById('root')
	)
);

if (module.hot) {
	module.hot.accept();
}


