import React from 'react';
import axios from "axios";
import { Link } from "react-router-dom";
import Container from "@material-ui/core/Container";
import { useStyles } from "../Home/styles";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Spinner from "../Component/Spinner";
import FooterCusto from "../Component/FooterCusto";
import {Helmet} from "react-helmet";
import Categories from "../Categories/Categories";
import slider1 from "../Assets/slider1.jpeg";
import PickerForm from "../Home/PickerForm";

const BASE = ATYPIC_CONFIG.endpoint_url;

class SeePost extends React.Component{
    async componentDidMount() {
        await this.getPost();
    }

    state = {
        post: [],
        loading: false
    };

    async getPost() {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/admin/posts/' + this.props.match.params.id);
            if (response && response.status === 200) {
                this.setState({ post: response.data[0], loading: false });
                console.log(response)
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    render() {
        return <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>{this.state.post.title}</title>
            </Helmet>
            <Container style={{ marginTop: "10%" }} className={useStyles.cardGrid} >

                <section className="banner d-flex align-items-center" style={{
                    backgroundImage: `url( https://imageatypic.s3.us-east-1.amazonaws.com/${this.state.post.image})`
                }}>
                </section>
                <div style={{display : "flex", justifyContent : "center", marginBottom : "20px"}}><h1 style={{color: "#ab3535"}}>{this.state.post.title}</h1></div>
                <div className="container-desc">
                    <div className="descp-text" style={{}}>
                        <p style={{ textAlign: "justify" }}>{this.state.post.content}</p>

                    </div>
                </div>
            </Container>
            {this.state.loading ? "" : <FooterCusto />}
        </>
    }
}

export default SeePost;