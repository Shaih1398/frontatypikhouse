import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';



const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            backgroundColor :"#61B345"
        },
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

export default function FloatingActionButtons(props) {
    const classes = useStyles();

    return (
        <div  className={classes.root} style={{display : "flex"}}>
            <Fab   onClick={(event) => {props.onChange()}}   color="primary" aria-label="add">
                {props.children}
            </Fab>
        </div>
    );
}
