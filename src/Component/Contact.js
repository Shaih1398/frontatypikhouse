import React, {useState} from "react";
import {Helmet} from "react-helmet";
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import TextField from "@material-ui/core/TextField";
import FooterCusto from "./FooterCusto";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Alert from "@material-ui/lab/Alert";
import {ATYPIC_CONFIG} from "../Auth/Config";
const BASE = ATYPIC_CONFIG.endpoint_url;



export default function Contact(props) {
    const [email, setEmail] = useState('');
    const [content, setContent] = useState('');
    const [showValidate, setShowvalidate] = useState(false);
    const [showValidateMsg, setshowValidateMsg] = useState('');
    const [showErrorEmail, setShowErrorEmail] = useState(false);
    const [showErrorContent, setShowErrorContent] = useState(false);
    const [errorMsgEmail, setErrorMsgEmail] = useState('');
    const [errorMsgContent, setErrorMsgContent] = useState('');

    async function handleSubmit() {

        if (!email) {
            setShowErrorEmail(true);
            setErrorMsgEmail('Veuillez remplir votre Mail')
        }
        else {
            setShowErrorEmail(false);
        }
        if (!content) {
            setShowErrorContent(true);
            setErrorMsgContent('Veuillez remplir votre message')
        }
        else {
            setShowErrorContent(false);
        }


        if (email && content) {
          await sendMail (email, content)
        }
    }
    async function sendMail(email, content) {

        try {
            //const response = await axios.post('http://atypic-env.eba-cmcp2v2h.us-east-1.elasticbeanstalk.com/api/auth/signin', {
            //http://localhost:8080
            const response = await axios.post(BASE + '/sendMailContact', {
                "email": email,
                "content": content
            });

            if (response && response.status === 200) {
                setShowvalidate(true);
                setshowValidateMsg("Votre message a bien été envoyé !")
                //if (response.data.isValid == true) {

            }
        } catch (error) {
            console.log(error);
        }
    }
    return ( <>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Nous contacter</title>
            </Helmet>

            <div style={{marginTop : "12%"}} align= "center">
                <div>
                    <h3>CONTACTEZ NOUS </h3>
                </div>
                <div style={{width :"17%"}}>
                    <img style={{ width: "100%" }} alt="logo AtypikHouse" src={logoAtypik} />
                </div>
                <div style={{marginTop : "1%"}}>
                    <b><span>ATYPIKHOUSE</span> </b>
                </div>
                <div style={{marginTop : "1%"}}><b>
                    <span>7 places hôtel de ville OISE 60350</span> </b>
                </div>
                <div style={{marginTop : "1%"}}>
                    <b><span>09 85 85 85 85</span> </b>
                </div>
                <div>
                    <TextField style={{width :"34%"}}
                    variant="outlined" onChange={e => setEmail(e.target.value)} margin="normal"  id="email" label="Votre mail" name="email" autoComplete="email" autoFocus
                    />
                    {showErrorEmail ?
                        <Alert severity="error" style={{ width: "34%" }}>
                            {errorMsgEmail}
                        </Alert> : ''
                    }
                </div>
                <div style={{marginTop :"2%", marginBottom : "1%"}}>
                    <TextField
                        id="outlined-multiline-static"
                        label="Votre message"
                        multiline
                        rows={10}
                        variant="outlined"
                        placeholder="Votre message"
                        style ={{width : "34%"}}
                        onChange={e => setContent(e.target.value)}
                    />
                    {showErrorContent ?
                        <Alert severity="error" style={{ width: "34%" }}>
                            {errorMsgContent}
                        </Alert> : ''
                    }
                </div>
                <div>
                    <Button onClick={() => handleSubmit() } style={{ width: "34%", marginBottom : "1%" }}  variant="contained" color="primary">
                        Envoyer
                    </Button>
                    {showValidate ?
                        <Alert severity="success" style={{ width: "34%" }}>
                            {showValidateMsg}
                        </Alert> : ''
                    }

                </div>
                <FooterCusto></FooterCusto>
            </div>
        </>
    )
}