import React from "react";
import Badge from "@material-ui/core/Badge";
import { withStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";


const StyledBadge = withStyles((theme) => ({
    badge: {


        border: `2px solid #61B345`,
        backgroundColor : '#61B345',
        color : 'white',
        cursor : 'inherit'

    }
}))(Badge);

export default function BadgeItem(props) {
    return (
        <IconButton aria-label="cart">
            <StyledBadge badgeContent={props.content}></StyledBadge>
        </IconButton>
    );
}
