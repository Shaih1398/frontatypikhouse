import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';




const useStyles = makeStyles({
    root: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
});

export default function HoverRating(props) {



    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Rating
                readOnly= {props.readOnly}
                name="hover-feedback"
                value={props.value}
                precision={0.5}
                onChange={(event, newValue) => {
                    props.onChange(newValue)

                }}


            />

        </div>
    );
}
