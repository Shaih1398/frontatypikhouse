import React from "react";
import "../css/spinner.css";

const DIMENSION = 33;

export default function Spinner({dimension, center, position, right, left, top, bottom, ...rest}) {
	let style = {
		width: (dimension || DIMENSION) + "px",
		height: (dimension || DIMENSION) + "px",
		objectFit: "contain",
		margin : "auto",
		animation: "spinnerRotate 1s infinite linear",
		position: "fixed",
		top: "0",
		left: "0",
		bottom: "0",
		right: "0",

	};
	return <img alt="spinner" {...rest} src="/icons/svg/icon-loader.svg" style={style} />;
}
