import React from 'react';
import {useStyles} from "../Home/styles";
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import './FooterCusto.css';
import {Link} from "react-router-dom";



export default function FooterCusto(props) {

    return(
        <footer style = {{backgroundColor :"#413122", position : "sticky"}} className={useStyles.footer}>
            <div className="c-res" style={{display :"flex",  padding :"20px", justifyContent : "space-between"}}>

                <div>
                    <div className="c-res" style={{width : "100%", display : "flex"}}>  <span style={{color :"white"}}>© 2021 AtypikHouse </span> </div>
                    <div className="c-res m-0" style={{display : "flex",  marginTop : "10px"}}>
                        <div className="m-CGU" style={{color :"white" , marginRight : "10px", fontSize :"13px"}}><Link to={"/nous-contacter"}><span>Contactez-nous</span></Link></div>
                        <div className="m-CGU" style={{color :"white", marginRight : "10px", fontSize :"13px" }}><Link to={"/CGV"}><span>Condition générale de vente</span></Link></div>
                        <div className="m-CGU" style={{color :"white", marginRight : "10px", fontSize :"13px" }}><Link to={"/mention-legales"}><span>Mentions légales</span> </Link></div>
                        <div className="m-CGU" style={{color :"white", fontSize :"13px"  }}><Link to={"/CGU"}><span>Condition générale d'utilisation  </span> </Link></div>
                    </div>
                </div>
                <div>

                </div>
                <div  className= "m-RS" style={{display : "flex", marginTop :"10px"}}>
                    <div style={{marginRight : "20px"}}><a href="https://www.facebook.com/Atypik-House-110319598046756/"><FacebookIcon style={{color :"white"}}/></a>   </div>
                    <div  style={{marginRight : "20px"}}><InstagramIcon style={{color :"white"}}/></div>
                    <div style={{marginRight : "20px"}}><TwitterIcon style={{color :"white"}}/></div>
                </div>

            </div>

        </footer>
    )

}