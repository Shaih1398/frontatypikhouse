import React from 'react';
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import FooterCusto from "./FooterCusto";

export default function MentionLegal(props) {
    return (<>
        <div  style={{marginTop :"8%", minHeight : "800px"}}>
            <div  style={{width :"20%", marginLeft :"43%"}}> <img style={{width : "70%"}} alt="logo AtypikHouse" src={logoAtypik}/></div>
            <h3 align ="center">MENTIONS LÉGALES</h3>
            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
               <strong> Identification de l'éditeur et de l'hébergeur du site </strong>
            </p>

            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                Le site http://www.AtypikHouse.com est édité par AtypikHouse (projet étudiant)
            </p>

            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                AtypikHouse est une SARL au capital de 10 000 €
            </p>
            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                Entreprise immatriculée au RCS de OISE sous le numéro 123456789
            </p>
            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                Le siège social se situe au :  7 places hôtel de ville, 60350 Oise
            </p>



            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                N° de TVA intracommunautaire : 123453555324
            </p>

            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                Directeur de la publication : Shai HADDAD, étudiant de AtypikHouse ( projet étudiant) SARL, joignable au 0610983643 ou à l'adresse shaihaddad@outlook.fr
            </p>

            <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                Le site est hébergé par AWS. Les informations concernant la collecte et le traitement des données personnelles (politique et déclaration) sont fournies dans la charte de données personnelles du site.
            </p>


        </div>
            <FooterCusto></FooterCusto>
        </>

    )
}