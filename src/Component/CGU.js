import React from 'react';
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import FooterCusto from "./FooterCusto";

export default function CGU(props) {
    return (<>
            <div  style={{marginTop :"8%", minHeight : "800px"}}>
                <div  style={{width :"20%", marginLeft :"43%"}}> <img style={{width : "70%"}} alt="logo AtypikHouse" src={logoAtypik}/></div>
                <h3 align ="center">Conditions générales d'utilisation – Données personnelles
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                    <strong> Définitions </strong>
                </p>


                <li>
                    <ol><b>L'Éditeur : </b>:  La personne, physique ou morale, qui édite les services de communication au public en ligne.</ol>
                    <ol><b>Le Site : </b> L'ensemble des sites, pages Internet et services en ligne proposés par l'Éditeur.</ol>
                    <ol><b>L'utilisateur :  </b> : La personne utilisant le Site et les services.</ol>
                </li>





                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Nature des données collectées
                </h3>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    Dans le cadre de l'utilisation des Sites, l'Editeur est susceptible de collecter les catégories de données suivantes concernant ses Utilisateurs :
                    Données d'état-civil, d'identité, d'identification...
                </p>



                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Communication des données personnelles à des tiers</h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Sur la base des obligations légales, vos données personnelles pourront être divulguées en application d'une loi, d'un règlement ou en vertu d'une décision d'une autorité réglementaire ou judiciaire compétente. De manière générale, nous nous engageons à nous conformer à toutes les règles légales qui pourraient empêcher, limiter ou réglementer la diffusion d’informations ou de données et notamment à se conformer à la Loi n° 78-17 du 6 janvier
                    1978 relative à l'informatique, aux fichiers et aux libertés.Communication à des tiers pour sollicitation commerciale pour des produits et services équivalents
                    Si vous avez effectué un achat sur notre Site, nous pouvons, avec nos partenaires commerciaux et de manière occasionnelle, vous tenir informé de nos nouveaux produits, actualités et offres spéciales, par courriel, par courrier postal et par téléphone quant à des produits ou services similaires aux produits ou services qui ont fait l'objet de votre commande.

                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>

                    Viralité des conditions de réutilisation des données personnelles
                    Pas d'engagement sur la viralité des conditions et soumission à des obligations contractuelles spécifiques
                    En cas de communication de vos données personnelles à un tiers, l'Editeur se réserve le droit de fixer contractuellement avec vous les modalités de réutilisation des données.

                    Information préalable pour la communication des données personnelles à des tiers en cas
                    de fusion / absorption
                    Information préalable et possibilité d’opt-out avant et après la fusion / acquisition
                    Dans le cas où nous prendrons part à une opération de fusion, d’acquisition ou à toute autre forme de cession d’actifs, nous nous engageons à garantir la confidentialité de vos données personnelles et à vous informer avant que celles-ci ne soient transférées ou soumises à de nouvelles règles de confidentialité.



                    Agrégation des données
                    Agrégation avec des données non personnelles
                    Nous pouvons publier, divulguer et utiliser les informations agrégées (informations relatives à tous nos Utilisateurs ou à des groupes ou catégories spécifiques d'Utilisateurs que nous combinons de manière à ce qu'un Utilisateur individuel ne puisse plus être identifié ou mentionné) et les informations non personnelles à des fins d'analyse du secteur et du marché, de profilage démographique, à des fins promotionnelles et publicitaires et à d'autres fins commerciales.
                    Agrégation avec des données personnelles disponibles sur les comptes sociaux de l'Utilisateur
                    Si vous connectez votre compte à un compte d’un autre service afin de faire des envois croisés, ledit service pourra nous communiquer vos informations de profil, de connexion, ainsi que toute autre information dont vous avez autorisé la divulgation. Nous pouvons agréger les informations relatives à tous nos autres Utilisateurs, groupes, comptes, aux données personnelles disponibles sur l’Utilisateur.

                    Collecte des données d'identité
                    Inscription et identification préalable pour la fourniture du service
                    L’utilisation du Site nécessite une inscription et une identification préalable. Vos données nominatives (nom, prénom, adresse postale, e-mail, numéro de téléphone,...) sont utilisées pour exécuter nos obligations légales résultant de la livraison des produits et / ou des services, en vertu du Contrat de licence utilisateur final, de la
                    Limite de garantie, le cas échéant, ou de toute autre condition applicable. Vous ne fournirez pas de fausses informations nominatives et ne créerez pas de compte pour une autre personne sans son autorisation. Vos coordonnées devront toujours être exactes et à jour.

                    Collecte des données d'identification
                    Utilisation de l'identifiant de l'utilisateur pour proposition de mise en relation et offres commerciales
                    Nous utilisons vos identifiants électroniques pour rechercher des relations présentes par connexion, par adresse mail ou par services. Nous pouvons utiliser vos informations de contact pour permettre à d'autres personnes de trouver votre compte, notamment via des services tiers et des applications clientes. Vous pouvez télécharger votre carnet d'adresses afin que nous soyons en mesure de vous aider à trouver des connaissances sur notre réseau ou pour permettre à d'autres Utilisateurs de notre réseau de vous trouver. Nous pouvons vous proposer des suggestions, à vous et à d'autres Utilisateurs du réseau, à partir des contacts importés de votre carnet d’adresses.
                    Nous sommes susceptibles de travailler en partenariat avec des sociétés qui proposent des offres incitatives. Pour prendre en charge ce type de promotion et d'offre incitative, nous sommes susceptibles de partager votre identifiant électronique.
                    Collecte des données du terminal
                    Aucune collecte des données techniques
                    Nous ne collectons et ne conservons aucune donnée technique de votre appareil (adresse IP, fournisseur d'accès à Internet...).

                    Cookies
                    Durée de conservation des cookies
                    Conformément aux recommandations de la CNIL, la durée maximale de conservation des cookies est de 13 mois au maximum après leur premier dépôt dans le terminal de l'Utilisateur, tout comme la durée de la validité du consentement de l’Utilisateur à l’utilisation de ces cookies. La durée de vie des cookies n’est pas prolongée à chaque visite. Le consentement de l’Utilisateur devra donc être renouvelé à l'issue de ce délai.

                    Finalité cookies
                    Les cookies peuvent être utilisés pour des fins statistiques notamment pour optimiser les services rendus à l'Utilisateur, à partir du traitement des informations concernant la fréquence d'accès, la personnalisation des pages ainsi que les opérations réalisées et les informations consultées. Vous êtes informé que l'Éditeur est susceptible de déposer des cookies sur votre terminal. Le cookie enregistre des informations relatives à la navigation sur le service (les pages que vous avez consultées, la date et l'heure de la consultation...) que nous pourrons lire lors de vos visites ultérieures.
                    Opt-in pour le dépôt de cookies
                    Nous n'utilisons pas de cookies. Si nous devions en utiliser à l’avenir, vous en seriez informé préalablement et auriez la possibilité de désactiver ces cookies.

                    Conservation des données techniques
                    Durée de conservation des données techniques
                    Les données techniques sont conservées pour la durée strictement nécessaire à la réalisation des finalités visées
                    ci-avant.

                    Délai de conservation des données personnelles et d'anonymisation
                    Conservation des données pendant la durée de la relation contractuelle
                    Conformément à l'article 6-5° de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les données à caractère personnel faisant l'objet d'un traitement ne sont pas conservées au-delà du temps nécessaire à l'exécution des obligations définies lors de la conclusion du contrat ou de la durée prédéfinie de la relation contractuelle.
                    Conservation des données anonymisées au-delà de la relation contractuelle / après la suppression du compte
                    Nous conservons les données personnelles pour la durée strictement nécessaire à la réalisation des finalités décrites dans les présentes CGU. Au-delà de cette durée, elles seront anonymisées et conservées à des fins exclusivement statistiques et ne donneront lieu à aucune exploitation, de quelque nature que ce soit.
                    Suppression des données après suppression du compte
                    Des moyens de purge de données sont mis en place afin d'en prévoir la suppression effective dès lors que la durée de conservation ou d'archivage nécessaire à l'accomplissement des finalités déterminées ou imposées est atteinte. Conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous disposez par ailleurs d'un droit de suppression sur vos données que vous pouvez exercer à tout moment en prenant contact avec l'Éditeur.
                    Suppression des données après 3 ans d'inactivité
                    Pour des raisons de sécurité, si vous ne vous êtes pas authentifié sur le Site pendant une période de trois ans, vous recevrez un e-mail vous invitant à vous connecter dans les plus brefs délais, sans quoi vos données seront supprimées de nos bases de données.
                    Suppression du compte
                    Suppression du compte à la demande
                    L'Utilisateur a la possibilité de supprimer son Compte à tout moment, par simple demande à l'Éditeur OU par le menu de suppression de Compte présent dans les paramètres du Compte le cas échéant.
                    Suppression du compte en cas de violation des CGU
                    En cas de violation d'une ou de plusieurs dispositions des CGU ou de tout autre document incorporé aux présentes par référence, l'Éditeur se réserve le droit de mettre fin ou restreindre sans aucun avertissement préalable et à sa seule discrétion, votre usage et accès aux services, à votre compte et à tous les Sites.

                    Indications en cas de faille de sécurité décelée par l'Éditeur
                    Information de l'Utilisateur en cas de faille de sécurité
                    Nous nous engageons à mettre en oeuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au regard des risques d'accès accidentels, non autorisés ou illégaux, de divulgation, d'altération, de perte ou encore de destruction des données personnelles vous concernant. Dans l'éventualité où nous prendrions connaissance d'un accès illégal aux données personnelles vous concernant
                    stockées sur nos serveurs ou ceux de nos prestataires, ou d'un accès non autorisé ayant pour conséquence la réalisation des risques identifiés ci-dessus, nous nous engageons à :
                    • Vous notifier l'incident dans les plus brefs délais ;
                    • Examiner les causes de l'incident et vous en informer ;
                    Prendre les mesures nécessaires dans la limite du raisonnable afin d'amoindrir les effets négatifs et préjudices pouvant résulter dudit incident

                    Limitation de la responsabilité
                    En aucun cas les engagements définis au point ci-dessus relatifs à la notification en cas de faille de sécurité ne peuvent être assimilés à une quelconque reconnaissance de faute ou de responsabilité quant à la survenance de l'incident en question.

                    Transfert des données personnelles à l'étranger
                    Pas de transfert en dehors de l'Union européenne
                    L'Éditeur s'engage à ne pas transférer les données personnelles de ses Utilisateurs en dehors de l'Union européenne.
                    Modification des CGU et de la politique de confidentialité
                    En cas de modification des présentes CGU, engagement de ne pas baisser le niveau de confidentialité de manière substantielle sans l'information préalable des personnes concernées
                    Nous nous engageons à vous informer en cas de modification substantielle des présentes CGU, et à ne pas baisser le niveau de confidentialité de vos données de manière substantielle sans vous en informer et obtenir votre consentement.

                    Droit applicable et modalités de recours
                    Application du droit français (législation CNIL) et compétence des tribunaux
                    Les présentes CGU et votre utilisation du Site sont régies et interprétées conformément aux lois de France, et notamment à la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Le choix de la loi applicable ne porte pas atteinte à vos droits en tant que consommateur conformément à la loi applicable de votre lieu de résidence. Si vous êtes un consommateur, vous et nous acceptons de se soumettre à la compétence non-exclusive des juridictions françaises, ce qui signifie que vous pouvez engager une action relative aux
                    présentes CGU en France ou dans le pays de l'UE dans lequel vous vivez. Si vous êtes un professionnel, toutes les actions à notre encontre doivent être engagées devant une juridiction en France.
                    En cas de litige, les parties chercheront une solution amiable avant toute action judiciaire. En cas d'échec de ces tentatives, toutes contestations à la validité, l'interprétation et / ou l'exécution des présentes CGU devront être portées même en cas de pluralité des défendeurs ou d'appel en garantie, devant les tribunaux français.

                    Portabilité des données
                    Portabilité des données
                    L'Éditeur s'engage à vous offrir la possibilité de vous faire restituer l'ensemble des données vous concernant sur simple demande. L'utilisateur se voit ainsi garantir une meilleure maîtrise de ses données, et garde la possibilité de les réutiliser. Ces données devront être fournies dans un format ouvert et aisément réutilisable.
                    Ces CGU sont mises à disposition par Aboutinnovation et Privacy Tech selon les termes de la
                    Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International. </p>

            </div>
            <FooterCusto></FooterCusto>
        </>

    )
}