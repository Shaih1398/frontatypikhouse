import React from 'react';
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import FooterCusto from "./FooterCusto";

export default function CGV(props) {
    return (<>
            <div  style={{marginTop :"8%", minHeight : "800px"}}>
                <div  style={{width :"20%", marginLeft :"43%"}}> <img style={{width : "70%"}} alt="logo AtypikHouse" src={logoAtypik}/></div>
                <h3 align ="center">Condition générale de vente</h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem"}}>
                    <strong> Définitions </strong>
                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    Les présentes Conditions Générales de Vente (ci-après les « CGV ») sont proposées par AtypikHouse ( projet étudiant) SARL, au capital de 10000 €, entreprise immatriculée au RCS de OISE sous le numéro 123456789, dont le siège social est sis au 7 places hôtel de ville, 60350 Oise (ci-après « AtypikHouse ( projet étudiant) »).

                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    On désignera par la suite :
                </p>
                <li>
                    <ol><b>« Site » </b>: le site http://www.AtypikHouse.com et l'ensemble de ses pages.</ol>
                    <ol><b>« Produits » </b> ou « Services » : l'ensemble des produits (matériels) et services (prestations) qu'il est possible d'acheter ou auxquels il est possible de souscrire sur le Site.</ol>
                    <ol><b>« Vendeur » </b> : AtypikHouse ( projet étudiant), personne morale ou physique, proposant ses Produits ou Services sur le Site.</ol>
                    <ol> <b>« Client » </b> : l'internaute, particulier ou professionnel, effectuant un achat de Produit(s) ou Service(s) sur le Site.</ol>
                    <ol><b>« Consommateur » </b>, conformément à la définition de l'article préliminaire du Code de la consommation : « toute personne physique qui agit à des fins qui n'entrent pas dans le cadre de son activité commerciale, industrielle, artisanale ou libérale ».</ol>
                </li>


                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    L'internaute visitant le Site et intéressé par les Produits et Services proposés par le Vendeur est invité à lire attentivement ces CGV, à les imprimer et/ou à les sauvegarder sur un support durable, avant de procéder à une commande sur le Site.
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem",marginRight :"1rem"}}>
                    Le Client reconnaît avoir pris connaissance des CGV et les accepte intégralement et sans réserve.
                </p>

                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Article 1 - Application des CGV et objet du Site</h3>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    Le Vendeur se réserve le droit de modifier à tout moment les CGV en publiant une nouvelle version de ces dernières sur le Site. Les CGV applicables au Client sont celles en vigueur au jour de sa commande sur le Site.

                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem"}}>
                    Les informations légales concernant l'hébergeur et l'éditeur du Site, la collecte et le traitement des données personnelles et les conditions d'utilisation du Site sont fournies dans les conditions générales d'utilisation, les mentions légales et la charte de données du présent Site.

                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem",marginRight :"1rem"}}>
                    Le présent Site propose la vente en ligne de Logement.

                </p>

                <p style = {{marginLeft :"1rem", MarginTop : "1rem",marginRight :"1rem"}}>
                    Le Site est d’accès libre et gratuit à tout Client. L'acquisition d'un Produit ou d’un Service suppose l’acceptation, par le Client, de l’intégralité des présentes CGV, qui reconnaît du même fait en avoir pris pleinement connaissance. Cette acceptation pourra consister par exemple, pour le Client, à cocher la case correspondant à la phrase d'acceptation des présentes CGV, ayant par exemple la mention « Je reconnais avoir lu et accepté l’ensemble des conditions générales du Site ». Le fait de cocher cette case sera réputé avoir la même valeur qu’une signature manuscrite de la part du Client.
                    L'acceptation des présentes CGV suppose de la part des Clients qu'ils jouissent de la capacité juridique nécessaire pour cela. Si le Client est mineur ou ne dispose pas de cette capacité juridique, il déclare avoir l'autorisation d'un tuteur, d'un curateur ou de son représentant légal.
                    Le Client reconnaît la valeur de preuve des systèmes d'enregistrement automatique du Vendeur et, sauf pour lui d'apporter une preuve contraire, il renonce à les contester en cas de litige.
                    L'Editeur met à la disposition du Client, sur son Site, une charte de confidentialité spécifiant l’ensemble des informations afférentes à l’utilisation des données à caractère personnel du Client collectées par l'Editeur et aux droits dont le Client dispose vis-à-vis de ces données personnelles. La politique de confidentialité des données fait partie des CGV. L'acceptation des présentes CGV implique par conséquent l'acceptation de la politique de confidentialité des données.

                </p>
                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Article 2 - Création d'un compte client</h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    La création d’un « compte client » est un préalable indispensable à toute commande du Client sur le présent Site. A cette fin, le Client sera invité à fournir un certain nombre d’informations personnelles tels que ses nom et prénom, son adresse email, son adresse postale et son numéro de téléphone, cette liste n’étant pas exhaustive. A ce titre, le Client s’engage à fournir des informations exactes. Le Client est responsable de la mise à jour de ses données. Il doit donc aviser sans délai le Vendeur en cas de changement. Le Client est le seul responsable de la véracité, de l'exactitude et de la pertinence des données fournies.
                    Le Client inscrit au Site a la possibilité d'y accéder en se connectant grâce à ses identifiants (adresse e-mail définie lors de son inscription et mot de passe) ou éventuellement en utilisant des systèmes tels que des boutons de connexion tiers de réseaux sociaux. Le Client est entièrement responsable de la protection du mot de passe qu’il a choisi. Il est encouragé à utiliser des mots de passe complexes. En cas d’oubli de mot de passe, le Client a la possibilité d’en générer un nouveau. Ce mot de passe constitue la garantie de la confidentialité des informations contenues dans sa rubrique « mon compte » et le Client s'interdit donc de le transmettre ou de le communiquer à un tiers. A défaut, le Vendeur ne pourra être tenu pour responsable des accès non autorisés au compte d'un Client.
                    Le compte client permet au Client de consulter toutes ses commandes effectuées sur le Site. Si les données contenues dans la rubrique compte client venaient à disparaître à la suite d’une panne technique ou d’un cas de force majeure, la responsabilité du Vendeur ne pourrait être engagée, ces informations n’ayant aucune valeur probante mais uniquement un caractère informatif. Les pages relatives au compte client sont librement imprimables par le Client titulaire du compte en question mais ne constituent nullement une preuve, elles n’ont qu’un caractère informatif destiné à assurer une gestion efficace de ses commandes ou contributions par le Client.
                    Chaque Client est libre de fermer son compte sur le Site. Pour ceci, il doit adresser un e-mail au Vendeur en indiquant qu’il souhaite supprimer son compte. Aucune récupération de ses données ne sera alors possible.
                    Le Vendeur se réserve le droit exclusif de supprimer le compte de tout Client qui aurait contrevenu aux présentes CGV (notamment et sans que cet exemple n’ait un quelconque caractère exhaustif, lorsque le Client aura fourni sciemment des informations erronées, lors de son inscription et de la constitution de son espace personnel) ou encore tout compte inactif depuis au moins une année. Ladite suppression ne sera pas susceptible de constituer un dommage pour le Client qui ne pourra prétendre à aucune indemnité de ce fait. Cette exclusion n’est pas exclusive de la possibilité, pour le Vendeur, d’entreprendre des poursuites d’ordre judiciaire à l’encontre du Client, lorsque les faits l’auront justifié.
                </p>
                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Article 3 - Modalité de souscription des commandes et descriptif du processus d’achat</h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Les Produits et Services proposés sont ceux qui figurent dans le catalogue publié sur le Site. Ces Produits et Services sont proposés dans la limite des stocks disponibles. Chaque Produit est accompagné d’un descriptif établi par le Vendeur en fonction des descriptifs fournis par le fournisseur.
                    Les photographies des Produits du catalogue reflètent une image fidèle des Produits et Services proposés mais ne sont pas constitutives d’un engagement contractuel dans la mesure où elles ne peuvent pas assurer une parfaite similitude avec les Produits physiques.
                    On définira ci-dessous comme « Panier » l’objet immatériel regroupant l’ensemble des Produits ou Services sélectionnés par le Client du Site en vue d’un achat en ayant cliqué sur ces éléments. Afin de procéder à sa commande, le Client choisit le ou les Produit(s) qu’il souhaite commander en les ajoutant à son « Panier » dont le contenu peut être modifié à tout moment.
                    Dès lors que le Client estimera avoir sélectionné et ajouté à son panier tous les Produits qu’il souhaite acheter, il aura la possibilité, pour valider sa commande, d’accéder à son panier en cliquant sur le bouton prévu à cet effet. Il sera alors redirigé sur une page récapitulative sur laquelle lui seront communiqués le nombre et les caractéristiques des Produits commandés, ainsi que leur prix unitaire.
                    S’il souhaite valider sa commande, le Client devra cocher la case relative à la ratification des présentes CGV et cliquer sur le bouton de validation. Le Client sera alors redirigé sur une page dans laquelle il devra remplir les champs de formulaire de commande. Il devra dans ce dernier cas renseigner un certain nombre de données personnelles le concernant, nécessaires au bon déroulement de la commande.
                    Toutes les commandes passées sur le Site doivent être dûment remplies et doivent préciser ces informations nécessaires. Le Client pourra faire des changements, des corrections, des ajouts, ou annuler la commande, et ce, jusqu'à la validation de celle-ci.
                    Dès lors que le Client aura rempli le formulaire, il sera alors invité à effectuer son paiement avec les moyens de paiements listés dans la section de ces CGV relative aux paiements. Après quelques instants le Client se verra adresser un courrier électronique de confirmation de la commande, lui rappelant le contenu de la commande et le prix de celle-ci.
                    Les Produits vendus restent la propriété du Vendeur jusqu’à complet paiement de leur prix, conformément à la présente clause de réserve de propriété.
                </p>
                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>Article 4 - Prix et modalités de paiement</h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Sauf mention contraire, les prix figurant dans le catalogue sont des prix entendus en Euros toutes taxes comprises (TTC), tenant compte de la TVA applicable au jour de la commande et hors éventuelle participation aux frais de traitement et d'expédition.
                    AtypikHouse ( projet étudiant) se réserve le droit à tout moment de modifier ses prix et de répercuter, si applicable, tout changement du taux de TVA en vigueur sur le prix des Produits ou Services proposés sur le Site. Néanmoins, le prix figurant au catalogue le jour de la commande sera le seul applicable au Client.
                    Le Client peut passer commande sur le présent Site et peut effectuer son règlement par carte bancaire.Les paiements par carte bancaire se font au moyen de transactions sécurisées fournies par un prestataire de plateforme de paiement en ligne.
                    Le présent Site n’a accès à aucune donnée relative aux moyens de paiement du Client. Le paiement est effectué directement entre les mains de la banque ou du prestataire de paiement recevant le paiement du Client. En cas de paiement par chèque ou virement bancaire, les délais de livraison définis à l’article « Livraisons » des présentes CGV ne commencent à courir qu’à compter de la date de réception effective du paiement par le Vendeur, ce dernier pouvant en apporter la preuve par tous moyens. La disponibilité des Produits est indiquée sur le Site, dans la fiche descriptive de chaque Produit.
                    AtypikHouse ( projet étudiant) archivera les bons de commandes et les factures sur un support fiable et durable constituant une copie fidèle. Les registres informatisés seront considérés par les parties comme preuve des communications, commandes, paiements et transactions intervenus entre les parties.
                </p>
                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 5 - Droit de rétractation et formulaire de rétractation
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Conformément à l'article L.221-18 du Code de la consommation, et si le droit de rétractation est applicable, le Client Consommateur dispose d'un délai de quatorze (14) jours ouvrables à compter de la date de réception du Produit de sa commande ou de la conclusion du contrat pour les prestations de services, pour se rétracter. Il sera tenu de retourner tout Produit ne lui convenant pas et demander l'échange ou le remboursement sans pénalité, à l'exception des frais de retour, sous quatorze jours à compter de la réception par AtypikHouse ( projet étudiant) de la demande de remboursement.
                    Le Produit devra obligatoirement être retourné en parfait état. Le cas échéant, il devra être accompagné de tous ses accessoires. Le Client Consommateur peut trouver ci-dessous un formulaire type de rétractation pour une commande passée sur le Site, à adresser à AtypikHouse ( projet étudiant). Il est entendu que le Client supportera les frais de renvoi du Produit en cas de rétractation, ainsi que le coût de renvoi du Produit si celui-ci, en raison de sa nature, ne peut normalement être renvoyé par la Poste.
                    Si les obligations précédentes ne sont pas effectuées, le Client perdra son droit de rétractation et le Produit lui sera retourné à ses frais.
                    Il est recommandé au Client d’effectuer le retour par une solution permettant un suivi du colis. Dans le cas contraire, si le colis retourné ne parvenait pas au Vendeur, il ne sera pas possible de lancer une enquête auprès des services postaux afin de leur demander de localiser ce dernier.
                    Le remboursement sera effectué en utilisant le même moyen de paiement que celui choisi par le Client pour la transaction initiale, sauf accord exprès du Client pour que le Vendeur utilise un autre mode de paiement, et dans la mesure où le remboursement n'occasionne pas de frais pour le Client.
                    Le Vendeur se réserve également le droit de différer le remboursement jusqu'à réception du Produit ou aussi longtemps que le Client n'aura pas démontré qu'il a fait l'expédition du Produit, si une telle démonstration n'a pas eu lieu précédemment.
                    En cas de dépréciation des Produits résultant de manipulations autres que celles nécessaires pour établir la nature, les caractéristiques et le bon fonctionnement du ou des Produit(s), la responsabilité du Client pourra être engagée.
                    Conformément à l'article L221-5 du Code de la consommation, ("loi Hamon") de juin 2014, le Client Consommateur peut trouver ci-dessous un formulaire type de rétractation pour une commande passée sur le site :
                </p>
                <h3 style = {{ MarginTop : "1rem" }} align="center">Formulaire de rétractation</h3>
                <hr/>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    A l'attention de AtypikHouse ( projet étudiant) SARL, 7 places hôtel de ville, 60350, Oise
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify",marginRight :"1rem"}}>
                    Je/nous (*) vous notifie/notifions (*) par la présente ma/notre (*) rétractation du contrat portant sur la vente du bien (*)/pour la prestation de services (*) ci-dessous :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Commandé le (*)/reçu le (*) :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify",marginRight :"1rem"}}>
                    Nom du (des) Client(s) :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Adresse du (des) Client(s) :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Signature du (des) Client(s) (uniquement en cas de notification du présent formulaire sur papier) :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Date :
                </p>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify",marginRight :"1rem"}}>
                    (*) Rayez la mention inutile.
                </p>
                <h3 style = {{marginLeft :"1rem", MarginTop : "1rem", marginRight :"1rem" }}>
                    Article 6 - Garantie des Produits
                </h3>
                <h3 align="center" style = {{ MarginTop : "1rem" }}>
                    Dispositions légales à reproduire
                </h3>
                <hr/>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Lorsqu'il agit en garantie légale de conformité, le consommateur bénéficie d’un délai de deux ans à compter de la délivrance du bien pour agir ; il peut choisir entre la réparation ou le remplacement du bien, sous réserve des conditions de coût prévues par l’article L.217-9 du Code de la consommation ; sauf pour les biens d’occasion, il est dispensé de prouver l’existence du défaut de conformité du bien durant les six mois suivant la délivrance du bien, délai porté à 24 mois à compter du 18 mars 2016.
                    La garantie légale de conformité s’applique indépendamment de la garantie commerciale éventuellement consentie.
                    Le consommateur peut décider de mettre en œuvre la garantie contre les défauts cachés de la chose vendue au sens de l’article 1641 du Code civil, à moins que le vendeur n'ait stipulé qu'il ne sera obligé à aucune garantie ; dans l'hypothèse d'une mise en œuvre de cette garantie, l'acheteur a le choix entre la résolution de la vente ou une réduction du prix de vente conformément à l’article 1644 du Code civil. Il dispose d’un délai de deux années à compter de la découverte du vice.
                    Le report, la suspension ou l’interruption de la prescription ne peut avoir pour effet de porter le délai de prescription extinctive au-delà de vingt ans à compter du jour de la naissance du droit conformément à l'article 2232 du Code civil.

                    Tous les articles acquis sur le présent site bénéficient des garanties légales suivantes, prévues par le Code Civil ;
                </p>
                <h3 align="center" style = {{ MarginTop : "1rem" }}>
                    Garantie légale de conformité
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Selon les articles L.217-4 et suivants du Code de la consommation, le Vendeur est tenu de livrer un bien conforme au contrat conclu avec le Client Consommateur et de répondre des défauts de conformité existant pendant la délivrance du Produit. La garantie de conformité pourra s'exercer si un défaut devait exister le jour de la prise de possession du Produit. Toutefois, lorsque le défaut est apparu dans les 24 mois qui suivent cette date (ou dans les 6 mois si la commande a eu lieu avant le 18 mars 2016 ou que le Produit est vendu d'occasion), il est présumé remplir cette condition. Mais, conformément à l'article L.217-7 du Code de la Consommation, « le Vendeur peut combattre cette présomption si celle-ci n'est pas compatible avec la nature du [Produit] ou le défaut de conformité invoqué ».
                    En revanche, passé ce délai de 24 mois (ou de 6 mois si la commande a eu lieu avant le 18 mars 2016 ou que le produit est vendu d'occasion), il reviendra au Client de prouver que le défaut existait bien au moment de la prise de possession du Produit.
                    Conformément à l'article L.217-9 du Code de la consommation : « en cas de défaut de conformité l'acheteur choisit entre la réparation et le remplacement du bien. Toutefois, le vendeur peut ne pas procéder selon le choix de l'acheteur si ce choix entraîne un coût manifestement disproportionné au regard de l'autre modalité, compte tenu de la valeur du bien ou de l'importance du défaut. Il est alors tenu de procéder, sauf impossibilité, selon la modalité non choisie par l'acheteur ».
                </p>
                <h3 align="center" style = {{ MarginTop : "1rem" }}>
                    Garantie légale contre les vices cachés
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify",marginRight :"1rem"}}>
                    Selon les articles 1641 à 1649 du Code civil, le Client pourra demander l'exercice de la garantie de vices cachés si les défauts présentés n'apparaissaient pas lors de l'achat, étaient antérieurs à l'achat (et donc ne pas résulter de l'usure normale du Produit par exemple), et sont suffisamment graves (le défaut doit soit rendre le Produit impropre à l'usage auquel il est destiné, soit diminuer cet usage dans une mesure telle que l'acheteur n'aurait pas acheté le Produit ou ne l'aurait pas acheté à un tel prix s'il avait connu le défaut).
                    Les réclamations, demandes d'échange ou de remboursement pour un Produit non conforme doivent s’effectuer par courrier postal ou par mail aux adresses indiquées dans les mentions légales du site.
                    En cas de non-conformité d’un Produit livré, il pourra être retourné au Vendeur qui procédera à son échange. En cas d'impossibilité d'échange du Produit (Produit obsolète, rupture de stock, etc.) le Client sera remboursé par chèque ou virement du montant de sa commande. Les frais de la procédure d'échange ou de remboursement (notamment les frais de port de retour du Produit) sont alors à la charge du Vendeur.
                </p>
                <h3  style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 7 - Service Client
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Le service clientèle du présent Site est accessible par courrier électronique à l’adresse suivante : contact@atypikHouse.fr ou par courrier postal à l’adresse indiquée dans les mentions légales.
                    AtypikHouse ( projet étudiant) met aussi à disposition de ses Clients une hotline, ou assistance téléphonique, pour répondre à leurs questions. L’assistance téléphonique peut être contactée par téléphone au 010101010101 (numéro non surtaxé).
                </p>
                <h3  style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 8 - Responsabilité
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify",marginRight :"1rem"}}>
                    Le Vendeur AtypikHouse ( projet étudiant) ne saurait être tenu pour responsable de l'inexécution du contrat conclu due à la survenance d'un événement de force majeure. Concernant les Produits achetés, le Vendeur n'encourra aucune responsabilité pour tous dommages indirects du fait des présentes, perte d'exploitation, perte de profit, dommages ou frais, qui pourraient survenir.
                    Le choix et l'achat d'un Produit ou d'un Service sont placés sous l'unique responsabilité du Client. L'impossibilité totale ou partielle d'utiliser les Produits notamment pour cause d'incompatibilité du matériel ne peut donner lieu à aucun dédommagement, remboursement ou mise en cause de la responsabilité du Vendeur, sauf dans le cas d'un vice caché avéré, de non-conformité, de défectuosité ou d'exercice du droit de rétractation si applicable, c'est à dire si le Client n'est pas Client Consommateur et que le contrat passé pour acquérir le Produit ou le Service permet la rétractation, selon l'article L 221-18 et suivants du Code de la consommation.
                    Le Client admet expressément utiliser le Site à ses propres risques et sous sa responsabilité exclusive. Le Site fournit au Client des informations à titre indicatif, avec des imperfections, erreurs, omissions, inexactitudes et autres ambivalences susceptibles d'exister. En tout état de cause, AtypikHouse ( projet étudiant) ne pourra en aucun cas être tenu responsable :
                    de tout dommage direct ou indirect, notamment en ce qui concerne les pertes de profits, le manque à gagner, les pertes de clientèle, de données pouvant entre autres résulter de l'utilisation du Site, ou au contraire de l'impossibilité de son utilisation ;
                    d'un dysfonctionnement, d'une indisponibilité d'accès, d'une mauvaise utilisation, d'une mauvaise configuration de l'ordinateur du Client, ou encore de l'emploi d'un navigateur peu usité par le Client ;
                    du contenu des publicités et autres liens ou sources externes accessibles par les Clients à partir du Site.
                    Les photographies et visuels des Produits présentés sur le Site n’ont aucun caractère contractuel, la responsabilité du Vendeur ne saurait donc être engagée si les caractéristiques des Produits diffèrent des visuels présents sur le Site ou si ces derniers sont erronés ou incomplets.
                </p>
                <h3  style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 9 - Droits de propriété intellectuelle
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Tous les éléments du présent Site appartiennent au Vendeur ou à un tiers mandataire, ou sont utilisés par le Vendeur avec l'autorisation de leurs propriétaires.
                    Toute reproduction, représentation, adaptation des logos, contenus textuels, pictographiques ou vidéos, sans que cette énumération ne soit limitative, est rigoureusement interdite et s’apparente à de la contrefaçon.
                    Tout Client qui se rendrait coupable de contrefaçon serait susceptible de voir son compte supprimé sans préavis ni indemnité et sans que cette suppression ne puisse lui être constitutive d’un dommage, sans réserve d’éventuelles poursuites judiciaires ultérieures à son encontre, à l’initiative du Vendeur ou de son mandataire.
                    Le présent Site utilise des éléments (images, photographies, contenus) dont les crédits reviennent à : SRD digital.
                    Les marques et logos contenus dans le Site sont susceptibles d'être déposés par AtypikHouse ( projet étudiant), ou éventuellement par un de ses partenaires. Toute personne procédant à leurs représentations, reproductions, imbrications, diffusions et rediffusions encourt les sanctions prévues aux articles L.713-2 et suivants du Code de la propriété intellectuelle.
                </p>
                <h3  style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 10 - Indépendances des clauses
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Si une disposition des CGV est jugée illégale, nulle ou pour toute autre raison inapplicable, alors cette disposition sera réputée divisible des CGV et n'affectera pas la validité et l'applicabilité des dispositions restantes.
                    Ces présentes CGV remplacent tous accords antérieurs ou contemporains écrits ou oraux. Les CGV ne sont pas cessibles, transférables ou sous-licenciable par le Client lui-même.
                    Une version imprimée des CGV et de tous les avis donnés sous forme électronique pourra être demandée dans des procédures judiciaires ou administratives en rapport avec les CGV. Les parties conviennent que toute la correspondance relative à ces CGV doit être rédigée dans la langue française.
                </p>
                <h3  style = {{marginLeft :"1rem", MarginTop : "1rem" }}>
                    Article 11 - Droit applicable et médiation
                </h3>
                <p style = {{marginLeft :"1rem", MarginTop : "1rem", textAlign : "justify", marginRight :"1rem"}}>
                    Les présentes CGV sont régies par et soumises au droit Français.
                    Sauf dispositions d’ordre public, tous litiges qui pourraient survenir dans le cadre de l’exécution des présentes CGV pourront avant toute action judiciaire être soumis à l’appréciation de l'Editeur du Site en vue d’un règlement amiable.
                    Il est expressément rappelé que les demandes de règlement amiable ne suspendent pas les délais ouverts pour intenter les actions judiciaires. Sauf disposition contraire, d’ordre public, toute action judiciaire relative à l’exécution des présentes CGV devra être soumise à la compétence des juridictions du ressort du lieu du domicile du défendeur.
                    Médiation de la consommation
                    Selon l'article L.612-1 du Code de la consommation, il est rappelé que « tout consommateur a le droit de recourir gratuitement à un médiateur de la consommation en vue de la résolution amiable du litige qui l’oppose à un professionnel. A cet effet, le professionnel garantit au consommateur le recours effectif à un dispositif de médiation de la consommation ».
                    A ce titre AtypikHouse ( projet étudiant) propose à ses Clients Consommateurs, dans le cadre de litiges qui n'auraient pas trouvé résolution de manière amiable, la médiation d'un médiateur de la consommation, dont les coordonnées sont les suivantes :
                    MEDIATEUR DE LA CONSOMMATION AGREE - DEVIGNY MEDIATION
                    contact@devignymediation.fr
                    https://www.devignymediation.fr/consommateurs.php
                    Il est rappelé que la médiation n'est pas obligatoire mais uniquement proposée afin de résoudre les litiges en évitant un recours à la justice.
                </p>
                <p >Tous droits réservés - 28 avril 2021 </p>

            </div>
            <FooterCusto></FooterCusto>
        </>

    )
}