import React from 'react';
import {GoogleMap, withScriptjs, withGoogleMap, Marker} from "react-google-maps";



function Map(){
    return(
        <GoogleMap
            defaultZoom={10}
            defaultCenter={{lat : 49.34808999735307, lng : 2.978030016169896}}
        >
            <Marker
                position={{ lat: 49.34808999735307, lng: 2.978030016169896 }}
            />
        </GoogleMap>
    );
}



const WrappedMap = withScriptjs(withGoogleMap(Map));

export default function MapsCustoContact(){
    return <div style={{maxHeight :"100px"}}><div style={{ width :"100vw", height : "100vh", marginTop :"8%"}}>
        <WrappedMap googleMapURL = {`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyBxq5A3roujo2xZKDmUmPT846I_pcXqn5U`}
                    loadingElement = {<div style={{height :"100%"}}/>}
                    containerElement = {<div style={{height :"100%"}}/>}
                    mapElement = {<div style={{height :"60%"}}/>}>
            location = { {lat: -34.397, lng: 150.644 }}
        </WrappedMap>
    </div></div>

}
