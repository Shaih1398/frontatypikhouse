import React from 'react';
import {GoogleMap, withScriptjs, withGoogleMap, Marker} from "react-google-maps";
import './Map.css'


function Map(){
    return(
      <GoogleMap
          defaultZoom={15}
          defaultCenter={{lat : 48.8193107, lng : 2.4162805}}
      >
          <Marker
              position={{ lat: 48.8193107, lng: 2.4162805 }}
          />
      </GoogleMap>
    );
}



const WrappedMap = withScriptjs(withGoogleMap(Map));

export default function MapsCusto(){
    return <div className= "mapContainer">
    <WrappedMap googleMapURL = {`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyBxq5A3roujo2xZKDmUmPT846I_pcXqn5U`}
                       loadingElement = {<div style={{height :"100%"}}/>}
                       containerElement = {<div style={{height :"100%"}}/>}
                       mapElement = {<div style={{height :"60%"}}/>}>
                       location = { {lat: -34.397, lng: 150.644 }}
    </WrappedMap>
    </div>

}
