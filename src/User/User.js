import React from 'react';
import { ATYPIC_CONFIG } from "../Auth/Config";
import axios from "axios";

const BASE = ATYPIC_CONFIG.endpoint_url;

class User extends React.Component {

	async componentDidMount() {
		await this.getInfoUser(localStorage.getItem("token"));
	}


	async getInfoUser(token) {
		try {
			const response = await axios.post(BASE + '/user/me', {}, {
				headers: {
					'x-access-token': token //the token is a variable which holds the token
				}
			});
			if (response && response.status === 200) {
				localStorage.setItem("token", response.data.accessToken);
			} else {
				console.log("Pas connecté !");
			}
		} catch (error) {
			console.error(error);
		}
	}


	render() {
		return <div className="User">

		</div>
	}
}

export default User;
