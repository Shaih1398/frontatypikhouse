import React, { PureComponent } from "react";
import { Redirect, Route } from "react-router-dom";
import { checkTokenExpirationMiddleware } from "../Middleware/middleware";

export default class AdminRoute extends PureComponent {
    render() {
        const { component: Component, ...rest } = this.props;
        let checkIfTokenIsValid = checkTokenExpirationMiddleware();
        let isUserLoggedIn = localStorage.getItem("status") && localStorage.getItem("token") ? localStorage.getItem("status") : false;
        return <Route {...rest} render={props => (isUserLoggedIn && checkIfTokenIsValid ? <Component {...props} /> : <Redirect to={("/signin")} />)} />;
    }
}

