import React, {PureComponent} from "react";
import {Redirect, Route} from "react-router-dom";
import {checkTokenExpirationMiddleware} from "../Middleware/middleware";

/*
Toutes ces fonctions ne sont pas testées pour le moment
*/

export default class ProtectedRouter extends PureComponent {
	render() {
		const {component: Component, ...rest} = this.props;
		let checkIfTokenIsValid = checkTokenExpirationMiddleware();
		let isUserLoggedIn = localStorage.getItem("token") ? localStorage.getItem("token") : false;
		//TODO : Il faut tester si on récupère des informations du user aussi
		console.log("test condition protected router : ", isUserLoggedIn && checkIfTokenIsValid)
		return <Route {...rest} render={props => (isUserLoggedIn && checkIfTokenIsValid ? <Component {...props} /> : <Redirect to={("/signin")}/>)}/>;
	}
}


