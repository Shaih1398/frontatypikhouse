import React from "react";
import { Route, Switch } from "react-router-dom";
import SignIn from "../Auth/SignIn";
import SignUp from "../Auth/SignUp";
import MyAccount from "../Auth/MyAccount";
import Home from "../Home/Home";
import ProtectedRouter from "./ProtectedRoute";
import AdminRoute from "./AdminRoute";
import Categories from "../Categories/Categories";
import Category from "../Categories/Category";
import ProductDetail from "../Product/ProductDetail";
import ConfirmBooking from "../Booking/ConfirmBooking";
import AddHouse from "../Booking/AddHouse";
import MyReservations from "../Booking/MyReservations";
import MyHouses from "../Booking/MyHouses";
import confirmEmail from "../confirmEmail/confirmEmail";
import MentionLegal from "../Component/MentionLegal";
import NavBar from "../Home/NavBar";
import Contact from "../Component/Contact";
import UpdateBooking from "../Booking/UpdateBooking";
import SeeReservations from "../Product/SeeReservations";
import CGV from "../Component/CGV"
import CGU from "../Component/CGU"
import './app.css'
import AdminHome from "../Admin/AdminHome";
import Posts from "../Admin/Posts";
import CategoryAdmin from "../Admin/CategoryAdmin";
import SeePost from "../Component/seePost";


class AppRouting extends React.Component {
	render() {
		return <div>
			<NavBar isAuth={!!localStorage.getItem("token")} />
			<Switch>
				<Route exact path="/signin" component={SignIn} />
				<Route path="/signup" component={SignUp} />
				<ProtectedRouter path="/myaccount" component={MyAccount} />
				<Route path="/categories" component={Categories} />
				<Route path="/category/:id" component={Category} />
				<Route path="/product/:id" component={ProductDetail} />
				<Route path="/confirmBooking" component={ConfirmBooking} />
				<Route path="/post/:id" component={SeePost} />
				<Route path="/CGV" component={CGV} />
				<Route path="/CGU" component={CGU} />
				<Route path="/verifyEmail/:token" component={confirmEmail} />
				<ProtectedRouter path="/mes-reservations" component={MyReservations} />
				<ProtectedRouter path="/mes-houses" component={MyHouses} />
				<ProtectedRouter path="/updateBooking/:id" component={UpdateBooking} />
				<ProtectedRouter path="/seeReservationHouse/:id" component={SeeReservations} />
				<ProtectedRouter path="/addHouse" component={AddHouse} />
				<AdminRoute path="/admin/post" component={Posts} />
				<AdminRoute path="/admin/category" component={CategoryAdmin} />
				<AdminRoute path="/admin" component={AdminHome} />
				<Route path="/mention-legales" component={MentionLegal} />
				<Route path="/nous-contacter" component={Contact} />
				<Route path="/" component={Home} />
			</Switch>
		</div>
			;
	}
}

export default AppRouting;
