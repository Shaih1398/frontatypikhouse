import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import TextareaAutosize from "@material-ui/core/TextareaAutosize/TextareaAutosize";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button";

const BASE = ATYPIC_CONFIG.endpoint_url;

class Posts extends React.Component {

    state = {
        title: "",
        content: "",
        file: null
    }


    async componentDidMount() {
        let idObject = window.location.search.replace('?', '').split('&').reduce((r, e) => (r[e.split('=')[0]] = decodeURIComponent(e.split('=')[1]), r), {});
        let id = idObject.id;
        if (id) {
            this.setState({ id: id });
            await this.getPost(id);
        }
    }

    onChangeInput = (field) => {
        return (event) => {
            this.setState({
                [field]: event.target.value,
            });
        };
    };

    handleSubmit = async () => {
        this.setState({ loading: true });
        try {
            const formData = new FormData();
            for (let files of this.state.file) {
                formData.append('file', files);
            }

            const posts = {
                content: this.state.content,
                title: this.state.title
            };

            formData.append('post', JSON.stringify(posts));
            const response = await axios.post(BASE + "/admin/addPosts", formData);
            if (response && response.status === 200) {
                this.setState({
                    addPost: true,
                    messageAddPost: "Post ajouté !"
                });
            }
        } catch (e) {
            this.setState({ loading: false, error: "Erreur Serveur" });
            console.log("error : ", e)
        }
    }

    async getPost(id) {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/admin/posts/' + id);
            if (response && response.status === 200) {
                this.setState({
                    content: response.data[0].content,
                    title: response.data[0].title
                })
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }


    render() {
        return <div style={{ marginTop: "13%", maxWidth: "1000px" }}>
            <h1>AdminHome</h1>
            <h2>{!!this.state.id ? "Modifier un post" : "Ajouter un post"}</h2>
            <div>
                <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                           id="title"
                           label="Titre"
                           type="text"
                           className={"title"}
                           InputLabelProps={{ shrink: true }}
                           value={this.state.title}
                           onChange={this.onChangeInput("title")} />
            </div>
            <br />
            <br />
            <br />
            <label htmlFor="mon_select">Courte description: </label>
            <TextareaAutosize aria-label="minimum height" onChange={this.onChangeInput("content")} minRows={5} placeholder="Courte description" defaultValue={this.state.content} />
            <br />
            <label>Choisir une photo: </label>
            <div className="upload-btn-wrapper">
                <button  className="btn">Ajouter vos photo</button>
                <input  name="myfile" type="file" multiple onChange={event => this.setState({file: event.target.files})}/>
            </div>
            <br />
            <Button variant="contained" color="primary" onClick={() => this.handleSubmit()}>Enregistrer mon post</Button>
            {this.state.addPost && this.state.messageAddPost && <p>{this.state.messageAddPost}</p>}
        </div>
    }
}

export default Posts;
