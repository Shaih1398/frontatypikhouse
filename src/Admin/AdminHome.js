import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField/TextField";

const BASE = ATYPIC_CONFIG.endpoint_url;

class AdminHome extends React.Component {

    async componentDidMount() {
        await this.getUsers();
        await this.getComments();
        await this.getHouses();
        await this.getPosts();
        await this.getCategories();
        await this.getDynamicProperties();
    }

    state = {
        users: [],
        comments: [],
        posts: [],
        houses: [],
        loading: false,
        dynamicProperty: null,
        type: "string",
        categoryId: 1,
        nameEquipment: ""
    };

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({ value: event.target.value });
    };

    handleChangeList = (event, states) => {
        this.setState({ [states]: event.target.value });
    };

    async getUsers() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.get(BASE + '/admin/users');

            if (response && response.status === 200) {
                this.setState({ users: response.data.users, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }


    async getComments() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try{
            const response = await axios.get(BASE + '/admin/comments');
            if (response && response.status === 200) {
                this.setState({ comments: response.data.comments, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }


    async getHouses() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.get(BASE + '/admin/houses');

            if (response && response.status === 200) {
                this.setState({ houses: response.data.house, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async getPosts() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.get(BASE + '/admin/posts');

            if (response && response.status === 200) {
                this.setState({ posts: response.data, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async getDynamicProperties() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.get(BASE + '/admin/propertyDynamic');

            if (response && response.status === 200) {
                this.setState({ dynamicProperties: response.data, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async blockOrUnblockUser(id) {
        const token = await localStorage.getItem("token");

        this.setState({ loading: true });
        try {
            const response = await axios.put(BASE + '/admin/blockUser/' + id);

            if (response && response.status === 200) {
                this.setState({ blockUser: true, loading: false, blockUserMessage: "User bloqué" });
            } else {
                this.setState({ blockUser: true, loading: false, blockUserMessage: "Impossible de bloquer" });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async authorizeComment(id) {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");

        try {
            const response = await axios.put(BASE + '/admin/comment/' + id);

            if (response && response.status === 200) {
                this.setState({ commentAuth: true, loading: false, commentAuthMessage: "Comentaire ajouté" });
            } else {
                this.setState({ commentAuth: true, loading: false, commentAuthMessage: "Impossible d'ajouter" });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }


    async unblockHouse(id) {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.put(BASE + '/admin/unblockHouse/' + id);
            if (response && response.status === 200) {
                this.setState({ blockHouse: true, loading: false, blockHouseMessage: "House debloqué" });
            } else {
                this.setState({ blockHouse: true, loading: false, blockHouseMessage: "Impossible de débloqué cette house" })
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async deletePost(id) {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.delete(BASE + '/admin/deletePosts/' + id);

            if (response && response.status === 200) {
                this.setState({ deletePosts: true, loading: false, messagDeletePost: "Post supprimé ! " });
            } else {
                this.setState({ deletePosts: true, loading: false, messagDeletePost: "Impossible de supprimé le post" })
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async addEquipment() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            let equipment = { name : this.state.nameEquipment }
            const response = await axios.post(BASE + '/admin/addEquipment', equipment);

            if (response && response.status === 200) {
                this.setState({ addEquipment: true, loading: false, addEquipmentMessage: "Equipement ajouté" });
            } else {
                this.setState({ addEquipment: true, loading: false, addEquipmentMessage: "Impossible d'ajouter l'équipement" });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async addDynamicProperty() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        let dynamicProperty = {
            name: this.state.dynamicProperty,
            categoryId: this.state.categoryId,
            type: this.state.type
        }
        try {
            const response = await axios.post(BASE + '/admin/addPropertyDynamic', dynamicProperty);

            if (response && response.status === 200) {
                this.setState({ propertyDynamic: true, loading: false, propertyDynamicAdd: "Ajouté ! " });
            } else {
                this.setState({ propertyDynamic: true, loading: false, propertyDynamicAdd: "Impossible d'ajouter ! "});
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    async getCategories() {
        this.setState({ loading: true });
        const token = await localStorage.getItem("token");
        try {
            const response = await axios.get(BASE + '/category');

            if (response && response.status === 200) {
                this.setState({ categories: response.data, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }


    onChangeInput = (field) => {
        return (event) => {
            this.setState({
                [field]: event.target.value,
            });
        };
    };

    render() {
        return <div style={{ marginTop: "13%", maxWidth: "1000px" }}>
            <h1>AdminHome</h1>
            <p>Les utilisateurs :</p>
            <div>
                {!this.state.loading && this.state.users && this.state.users.map(user =>
                    <div style={{ padding: "20px 20px 20px", border: "1px solid black", margin: "10px" }}>
                        <p>ID : {user.id}</p>
                        <p>Nom : {user.firstName}</p>
                        <p>Prénom : {user.lastName}</p>
                        <p>Email : {user.email}</p>
                        <p>Rue : {user.street}</p>
                        <p>Code postal : {user.postalCode}</p>
                        <p>Status :{user.isBlock ? "Bloqué" : "Valide"}</p>
                        <Button variant="contained" color="primary" onClick={() => this.blockOrUnblockUser(user.id)}>
                            {user.isBlock ? "Débloqué " : "Bloqué"}
                        </Button>
                    </div>
                )}
                {this.state.blockUser && this.state.blockUserMessage && <p>{this.state.blockUserMessaage}</p>}
            </div>
            <br /><br /><br />

            <h2>COMMENTS :</h2>
            {!this.state.loading && this.state.comments && this.state.comments.map(comment =>
                <div style={{ padding: "10px", border: "1px solid black", margin: "10px" }}>
                    <p>ID : {comment.id}</p>
                    <p>rate : {comment.rate}</p>
                    <p>comment : {comment.comment}</p>
                    <Button variant="contained" color="primary" onClick={() => this.authorizeComment(comment.id)}>
                        Ajouter le commentaire
                    </Button>
                </div>
            )}
            {this.state.commentAuth && this.state.commentAuthMessage && <p>{this.state.commentAuthMessage}</p>}
            <br /><br /><br />
            <h2>Houses : </h2>
            {!this.state.loading && this.state.houses && this.state.houses.map(house =>
                <div style={{ padding: "10px", border: "1px solid black", margin: "10px" }}>
                    <p>ID : {house.id}</p>
                    <p>name : {house.name}</p>
                    <p>price : {house.price}</p>
                    <p>nbBed : {house.nbBed}</p>
                    <p>street : {house.street}</p>
                    <p>city : {house.city}</p>
                    <Button variant="contained" color="primary" onClick={() => this.unblockHouse(house.id)}>
                        Autoriser la house
                    </Button>
                </div>
            )}
            {this.state.blockHouse && this.state.blockHouseMessage && <p>{this.state.blockHouseMessage}</p>}

            <h2>Posts : </h2>
            <div style={{ marginTop: "20px" }}>
                <Link to={"/admin/post"}>  <span style={{ color: "#61B345", fontSize: "13px" }}>Ajouter un post </span> </Link>
            </div>
            {!this.state.loading && this.state.posts && this.state.posts.map(post =>
                <div style={{ padding: "10px", border: "1px solid black", margin: "10px" }}>
                    <p>ID : {post.id}</p>
                    <p>Title : {post.title}</p>
                    <p>Content : {post.content}</p>
                    <p>Image : {post.image}</p>
                    <Button variant="contained" color="primary" onClick={() => this.deletePost(post.id)}>
                        Supprimer le post
                    </Button>
                    <div style={{ marginTop: "20px" }}>
                        <Link to={"/admin/post?id=" + post.id}>  <span style={{ color: "#61B345", fontSize: "13px" }}>Modifier le post </span> </Link>
                    </div>
                </div>
            )}
            {this.state.deletePost && this.state.messagDeletePost && <p>{this.state.messagDeletePost}</p>}
            <h2>Les categories</h2>
            <div style={{ marginTop: "20px" }}>
                <Link to={"/admin/category"}>  <span style={{ color: "#61B345", fontSize: "13px" }}>Ajouter une catégorie </span> </Link>
            </div>
            {this.state.categories && this.state.categories.map(category =>
                <div style={{ padding: "10px", border: "1px solid black", margin: "10px" }}>
                    <p>Id : {category.id}</p>
                    <p>Name : {category.name}</p>
                    <div style={{ marginTop: "20px" }}>
                        <Link to={"/admin/category?id=" + category.id}>
                            <span style={{ color: "#61B345", fontSize: "13px" }}>Modifier la category </span>
                        </Link>
                    </div>
                </div>
            )}

            <h2>Les propriété dynamiques</h2>
            {this.state.dynamicProperties && this.state.dynamicProperties.map(property =>
                <div style={{ padding: "10px", border: "1px solid black", margin: "10px" }}>
                    <p>Id : {property.id}</p>
                    <p>Nom : {property.name}</p>
                    <p>Type : {property.type}</p>
                </div>
            )}


            <h2>Ajouter un equipement : </h2>
            <div>
                <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                           id="nameEquipment"
                           label="Equipement"
                           type="nameEquipment"
                           className={"nameEquipment"}
                           InputLabelProps={{ shrink: true }}
                           value={this.state.nameEquipment}
                           onChange={this.onChangeInput("nameEquipment")} />
            </div>
            <Button variant="contained" color="primary" onClick={() => this.addEquipment()}>
                Ajouter un équipement
            </Button>

            {this.state.addEquipment && this.state.addEquipmentMessage && <p>{this.state.addEquipmentMessage}</p>}
            <h2>Ajouter une propriété dynamique : </h2>
            <div>
                <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                           id="dynamicProperty"
                           label="Propriété dynamique"
                           type="dynamicProperty"
                           className={"dynamicProperty"}
                           InputLabelProps={{ shrink: true }}
                           value={this.state.dynamicProperty}
                           onChange={this.onChangeInput("dynamicProperty")} />
            </div>
            <div>
                <label htmlFor="mon_select">Type : </label>
                <select id="categorySelect" onChange={(e) => this.handleChangeList(e, "type")}>
                    <option value={"string"}>String</option>
                    <option value={"boolean"}>Boolean</option>
                    <option value={"int"}>Integer</option>
                </select>
            </div>
            <div>
                <label htmlFor="mon_select">Choisir une category : </label>
                <select id="categorySelect" onChange={(e) => this.handleChangeList(e, "categoryId")}>
                    {this.state.categories && this.state.categories.map(category =>
                        <option value={category.id}>{category.name}</option>)
                    }
                </select>
            </div>
            <Button variant="contained" color="primary" onClick={() => this.addDynamicProperty()}>
                Ajouter une propriété dynamique
            </Button>
        </div>
        {this.state.propertyDynamic && this.state.propertyDynamicAdd && <p>{this.state.propertyDynamicAdd}</p>}
    }

}

export default AdminHome;
