import React from 'react';
import axios from "axios";
import { ATYPIC_CONFIG } from "../Auth/Config";
import TextareaAutosize from "@material-ui/core/TextareaAutosize/TextareaAutosize";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button";

const BASE = ATYPIC_CONFIG.endpoint_url;

class CategoryAdmin extends React.Component {

    state = {
        name: "",
        file: null,
        dynamicProperties: [],
        id: null
    }


    async componentDidMount() {
        let idObject = window.location.search.replace('?', '').split('&').reduce((r, e) => (r[e.split('=')[0]] = decodeURIComponent(e.split('=')[1]), r), {});
        let id = idObject.id;
        if (id) {
            this.setState({ id: id });
            await this.getCategory(id);
        }
    }

    onChangeInput = (field) => {
        return (event) => {
            this.setState({
                [field]: event.target.value,
            });
        };
    };

    handleSubmit = async () => {
        if (!this.state.id) {
            this.setState({ loading: true });
            try {
                const category = {
                    name: this.state.name,
                };

                const formData = new FormData();
                if (this.state.file) {
                    for (let files of this.state.file) {
                        formData.append('file', files);
                    }
                }

                formData.append('category', JSON.stringify(category));
                const token = await localStorage.getItem("token");
                const response = await axios.post(BASE + "/admin/addCategory", formData, {
                    headers: { "Content-Type": "multipart/form-data;",  'x-access-token': token }
                });
                if (response && response.status === 200) {
                    console.log("DONE");
                    this.setState({ loading: false, message: "Catégory ajouté" });
                } else {
                    this.setState({ loading: false, error: "Erreur Serveur", message: "Catégory pas ajouté" });
                }

            } catch (e) {
                this.setState({ loading: false, error: "Erreur Serveur", message: "Catégory pas ajouté" });
                console.log("error : ", e)
            }
        } else {
            this.setState({ loading: true });
            try {
                const category = {
                    name: this.state.name,
                };
                const response = await axios.put(BASE + "/admin/updateCategById/" + this.state.id, category);
                if (response && response.status === 200) {
                    this.setState({ loading: false, message: "Catégory modifiée" });
                } else {
                    this.setState({ loading: false, error: "Erreur Serveur", message: "Catégory non modifiée" });
                }

            } catch (e) {
                this.setState({ loading: false, error: "Erreur Serveur", message: "Catégory non modifiée" });
                console.log("error : ", e)
            }
        }
    }

    async getCategory(id) {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/admin/category/' + id);
            if (response && response.status === 200) {
                console.log(response.data);
                this.setState({
                    name: response.data.name,
                    dynamicProperties: response.data.dynamicProperties
                })
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    handleChange = (event, states) => {
        this.setState({ [states]: event.target.value });
    };

    render() {
        return <div style={{ marginTop: "13%", maxWidth: "1000px" }}>
            <h1>AdminHome</h1>
            <h2>{!!this.state.id ? "Modifier une category" : "Ajouter une category"}</h2>
            <div>
                <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                           id="name"
                           label="Nom"
                           type="name"
                           className={"name"}
                           InputLabelProps={{ shrink: true }}
                           value={this.state.name}
                           onChange={this.onChangeInput("name")} />
            </div>
            <br />
            {!this.state.id && <div className="upload-btn-wrapper">
                <button className="btn">Ajouter une image</button>
                <input name="myfile" type="file" onChange={event => this.setState({ file: event.target.files })} />
            </div>}
            <br />

            <Button variant="contained" color="primary" onClick={() => this.handleSubmit()}>Enregistrer ma category</Button>
            {this.state.message && <p>{this.state.message}</p>}
        </div>
    }
}

export default CategoryAdmin;
