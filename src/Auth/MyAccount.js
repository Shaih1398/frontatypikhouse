import React from 'react';
import axios from 'axios';
import { ATYPIC_CONFIG } from "./Config";
import MyReservations from "../Booking/MyReservations";
import MyHouses from "../Booking/MyHouses";
import AddHouse from "../Booking/AddHouse";
import MyInformations from "../Booking/MyInformations";

const BASE = ATYPIC_CONFIG.endpoint_url;

class MyAccount extends React.Component {

	async componentDidMount() {
		await this.getInfoUser();
	}

	state = {
		infosUser: [],
		loading: false,
	};

	async getInfoUser() {
		this.setState({ loading: true });
		try {
			const token = await localStorage.getItem("token");
			const response = await axios.get(BASE + '/user/me', {
				headers: {
					'x-access-token': token
				}
			});

			if (response && response.status === 200) {
				this.setState({ infosUser: response.data, loading: false });
			} else {
				console.log("Pas connecté !");
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}


	render() {
		return <div>
			<div className="ConnectedPage">
				{
					this.state.loading ? <div> Loading ... </div> :
						<div className="MyReservations" style={{ margin: "10%", padding: "10px" }}>
							<MyInformations user={this.state.infosUser} />
						</div>
				}
			</div>
		</div>
	}
}

export default MyAccount;
