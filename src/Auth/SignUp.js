import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { ATYPIC_CONFIG } from "./Config";
import logoAtypik from "../Assets/Logo_AtypikHouse.png";
import FooterCusto from "../Component/FooterCusto";
import Alert from "@material-ui/lab/Alert";

const BASE = ATYPIC_CONFIG.endpoint_url;

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(3),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function SignUp() {
	const classes = useStyles();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [street, setStreet] = useState('');
	const [postalCode, setPostalCode] = useState('');
	const [city, setCity] = useState('');
	const [country, setCountry] = useState('');
	const [checked, setChecked] = React.useState(false);
	const [showSucces, setshowSucces] = React.useState(false);
	const [msgSucces, setshowMsgSucces] = React.useState(false);
	const history = useHistory();

	useEffect(() => {
		// Met à jour le titre du document via l’API du navigateur
		document.title = `S'inscrire`;
	});


	async function handleSubmit(event) {
		event.preventDefault();
		await signUp(firstName, lastName, street, city, country, postalCode, email, password);
	}


	// Want to use async/await? Add the `async` keyword to your outer function/method.
	async function signUp(firstName, lastName, street, city, country, postalCode, email, password) {
		try {
			//const response = await axios.post('http://atypic-env.eba-cmcp2v2h.us-east-1.elasticbeanstalk.com/api/auth/signup', {
			const response = await axios.post(BASE + '/auth/signup', {
				"firstName": firstName,
				"lastName": lastName,
				"street": street,
				"postalCode": postalCode,
				"city": city,
				"country": country,
				"email": email,
				"password": password
			});
			if (response && response.status === 200) {
				setshowSucces(true);
				setshowMsgSucces("Votre compte a bien été créé ! Veuillez activer votre adresse mail (vérifiez les spams)")
			}
		} catch (error) {
			console.error("error : ", error);
		}
	}

	const handleChange = (event) => {
		setChecked(event.target.checked);
	};

	return (<>
		<Container style={{ marginBottom: "8%" }} component="main" maxWidth="xs">
			<CssBaseline />
			<div className={classes.paper}>
				<div style={{ width: "50%" }}>
					<img style={{ width: "100%" }} alt="logo AtypikHouse" src={logoAtypik} />
				</div>
				<Typography component="h1" variant="h5">
					Se connecter
				</Typography>
				<form className={classes.form} noValidate onSubmit={handleSubmit}>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<TextField
								autoComplete="fname"
								name="firstName"
								variant="outlined"
								required
								fullWidth
								id="firstName"
								label="Prénom"
								autoFocus
								onChange={e => setFirstName(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="lastName"
								label="Nom de famille"
								name="lastName"
								autoComplete="lname"
								onChange={e => setLastName(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="street"
								label="Rue"
								name="street"
								autoComplete="street"
								onChange={e => setStreet(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="city"
								label="Ville"
								name="city"
								autoComplete="city"
								onChange={e => setCity(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="country"
								label="Pays"
								name="country"
								autoComplete="country"
								onChange={e => setCountry(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="postalCode"
								label="Code postal"
								name="postalCode"
								autoComplete="postalCode"
								onChange={e => setPostalCode(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="email"
								label="Adresse email"
								name="email"
								autoComplete="email"
								onChange={e => setEmail(e.target.value)}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								name="password"
								label="Mot de passe"
								type="password"
								id="password"
								autoComplete="current-password"
								onChange={e => setPassword(e.target.value)}
							/>
						</Grid>

						<Grid item xs={12}>
							<FormControlLabel
								control={<Checkbox value="allowExtraEmails" color="primary" checked={checked}
								onChange={handleChange}/>}
								label="J'accepte les conditions générales d'utilisation"
							/>
						</Grid>
					</Grid>
					{checked && <Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
					>
						S'inscrire
					</Button>}
					{showSucces ?
						<Alert severity="success" style={{ width: "100%" }}>
							{msgSucces}
						</Alert> : ''
					}
					<Grid container justify="flex-end">
						<Grid item>
							<Link href="/SignIn" variant="body2">
								Vous avez déjà un compte ? Connectez-vous !
							</Link>
						</Grid>
					</Grid>
				</form>
			</div>


		</Container>
		<FooterCusto></FooterCusto>
	</>
	);
}
