import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { ATYPIC_CONFIG } from "./Config";
import logoAtypik from "../Assets/Logo_AtypikHouse.png"
import Alert from '@material-ui/lab/Alert';
import FooterCusto from "../Component/FooterCusto";

const BASE = ATYPIC_CONFIG.endpoint_url;

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function SignIn() {

	useEffect(() => {
		// Met à jour le titre du document via l’API du navigateur
		document.title = `Se connecter`;
	});


	const classes = useStyles();
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [showError, setShowError] = useState(false);
	const [showErrorEmail, setShowErrorEmail] = useState(false);
	const [showErrorPsw, setShowErrorPsw] = useState(false);
	const [showErrorBlock, setShowErrorBlock] = useState(false);
	const [blockErrorMsg, setblockErrorMsg] = useState('');
	const [errorMsg, setErrorMsg] = useState('');
	const [errorMsgEmail, setErrorMsgEmail] = useState('');
	const [errorMsgPsw, setErrorMsgPsw] = useState('');

	const history = useHistory();

	async function handleSubmit(event) {
		event.preventDefault();
		if (!password) {
			setShowErrorPsw(true);
			setErrorMsgPsw('Veuillez remplir votre mot de passe')
		}
		else {
			setShowErrorPsw(false);
		}
		if (!email) {
			setShowErrorEmail(true);
			setErrorMsgEmail('Veuillez remplir votre adresse mail')
		}
		else {
			setShowErrorEmail(false);
		}


		if (email && password) {
			await login(email, password);
		}


	}

	// Want to use async/await? Add the `async` keyword to your outer function/method.
	async function login(email, password) {

		try {
			//const response = await axios.post('http://atypic-env.eba-cmcp2v2h.us-east-1.elasticbeanstalk.com/api/auth/signin', {
			//http://localhost:8080
			const response = await axios.post(BASE + '/auth/signin', {
				"email": email,
				"password": password
			});

			if (response && response.status === 200) {
				console.log(response.data)
				//if (response.data.isValid == true) {
				if (response.data && !!response.data.isValid) {
					if (response.data.isBlock == false){
					localStorage.setItem("token", response.data.accessToken);
					const checkAdminRoles = response.data.roles.includes("ROLE_ADMIN");
					checkAdminRoles ? localStorage.setItem("status", "admin") : null;
					window.location.replace("/");
					}
					else{
						setShowErrorBlock(true);
						setblockErrorMsg('Votre compte à été bloqué par un administrateur !')
					}
				}
				else {
					setShowError(true);
					setErrorMsg('Veuillez vérifier votre adresse e-mail')
				}
			} else {
				setShowError(true);
				setErrorMsg('Compte introuvable ! Merci de vérifier votre adresse mail et votre mot de passe')
			}
		} catch (error) {
			setShowError(true);
			setErrorMsg('Compte introuvable ! Merci de vérifier votre adresse mail et votre mot de passe')
			console.error(error, showError);
		}
	}


	return (<>
		<Container style={{ marginTop: "80px", marginBottom: "8%" }} component="main" maxWidth="xs">
			<CssBaseline />
			<div className={classes.paper}>

				<div style={{ width: "50%" }}>
					<img style={{ width: "100%" }} alt="logo AtypikHouse" src={logoAtypik} />
				</div>

				<Typography component="h1" variant="h5">
					Sign in
				</Typography>
				<form className={classes.form} onSubmit={handleSubmit}>
					<TextField
						variant="outlined" margin="normal" fullWidth id="email" label="Email Address" name="email" autoComplete="email" autoFocus
						onChange={e => setEmail(e.target.value)}
					/>
					{showErrorEmail ?
						<Alert severity="error" style={{ width: "100%" }}>
							{errorMsgEmail}
						</Alert> : ''
					}

					<TextField
						variant="outlined" margin="normal" fullWidth name="password" label="Password" type="password" id="password" autoComplete="current-password"
						onChange={e => setPassword(e.target.value)}
					/>
					{showErrorPsw ?
						<Alert severity="error" style={{ width: "100%" }}>
							{errorMsgPsw}
						</Alert> : ''
					}
					{showError ?
						<Alert severity="error" style={{ width: "100%" }}>
							{errorMsg}
						</Alert> : ''
					}


					<Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
						Se connecter
					</Button>
					{showErrorBlock ?
						<Alert severity="error" style={{ width: "100%" }}>
							{blockErrorMsg}
						</Alert> : ''
					}
					<Grid container>

						<Grid item>
							<Link href="/signup" variant="body2">
								{"Vous n'avez pas de compte ? Inscrivez-vous ! "}
							</Link>
						</Grid>
					</Grid>
				</form>
			</div>

		</Container>
		<FooterCusto></FooterCusto>
	</>
	);
}
