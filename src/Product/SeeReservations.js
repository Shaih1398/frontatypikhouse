import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import TextareaAutosize from "@material-ui/core/TextareaAutosize/TextareaAutosize";
import TextField from '@material-ui/core/TextField';
import RoomIcon from '@material-ui/icons/Room';
import Autocomplete from "@material-ui/lab/Autocomplete";
import { loadStripe } from "@stripe/stripe-js";
import axios from "axios";
import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import { ATYPIC_CONFIG } from "../Auth/Config";
import FooterCusto from "../Component/FooterCusto";
import Spinner from "../Component/Spinner";
import StarRating from '../Component/StarRating';
import { useStyles } from "../Home/styles";
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import './ProductDetail.css';
import './slider.css';
import { makeStyles } from '@material-ui/core/styles';
import {Helmet} from "react-helmet";



const BASE = ATYPIC_CONFIG.endpoint_url;

const promise = loadStripe(ATYPIC_CONFIG.publicKeyStripesTest);

const useStylesPaper = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(1),
            width: theme.spacing(16),
            height: theme.spacing(16),
        },
    },
}));

class SeeReservations extends React.Component {

    state = {
        house: [],
        loading: false,
        open: false,
        picker1: "",
        picker2: "",
        validPicker: false,
        textValid: "",
        numberOfNight: 0,
        totalPrice: 0,
        isVisiblePay: false,
        seeComments: false,
        numberComment: 0,
        floatButtonIcon: "+",
        geolocationGoogle: {},
        seeBooking: false,
        delete: false,
        equipments: [],
        selectedDays: []
    };

    async componentDidMount() {
        await this.getHouse();
        await this.getEquipments();
    }

    onChangeInput = (field) => {
        return (event) => {
            this.setState({
                [field]: event.target.value,
            });
        };
    };



    getDateFormattingForField() {
        return new Date().toISOString().split('T')[0]
    }

    async getHouse() {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/house/' + this.props.match.params.id);
            if (response && response.status === 200) {
                this.setState({ house: response.data[0], loading: false });
                this.setState({ name: response.data[0].name });
                this.setState({ shortDesc: response.data[0].shortDesc });
                this.setState({ longDesc: response.data[0].name });
                this.setState({ price: response.data[0].price });
                this.setState({ nbBed: response.data[0].nbBed });
                this.setState({ receptionCapacity: response.data[0].receptionCapacity });
                this.setState({ showcomments: this.state.house.comments.length })
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            this.setState({ loading: false });
        }
    }

    getPopIn() {
        const range = {
            after: Date.now(),

        }
        return <div className={"c-booking"}>
            <h3 style={{ color: "#ab3535" }}>Choisir des dates d'indisponibilité</h3>
            <DayPicker
                selectedDays={this.state.selectedDays}
                onDayClick={this.handleDayClick}
                disabledDays={[
                    {

                        before: new Date(),
                    },
                ]}
            />
            <div align = "center" style={{marginTop : "2%"}}>
            <Button style={{ marginTop: "20px", width: "50%" }} variant="contained" color="primary" onClick={() => this.handleClose()}>Confirmer</Button>
            </div>
        </div>
    }




    handleDayClick = (day, { selected }) => {
        const selectedDays = this.state.selectedDays.concat();
        if (selected) {
            const selectedIndex = selectedDays.findIndex(selectedDay =>
                DateUtils.isSameDay(selectedDay, day)
            );
            selectedDays.splice(selectedIndex, 1);
        } else {
            selectedDays.push(day);
        }
        console.log(selectedDays);
        this.setState({ selectedDays });
    }

    async handleClose() {
        if (this.state.selectedDays) {
            try {
                const token = await localStorage.getItem("token");
                const response = await axios.post(BASE + '/unavailability/house', {
                    date: this.state.selectedDays,
                    houseId: this.state.house.id
                },
                    {
                        headers: {
                            'x-access-token': token,
                            "Content-Type": "application/json"
                        }
                    });


                if (response && response.status === 200) {
                    history.push("/myaccount");
                }
            } catch (error) {
                console.error("error : ", error);
            }
        }
    }

    getAvererageGradeRate(comments) {
        const mapArray = comments.map(comment => comment.rate);
        return mapArray.reduce((a, b) => a + b) / mapArray.length;
    }

    formatDate(date) {
        return new Date(date).toLocaleDateString();
    }

    handleSubmit = async () => {

        const house = {
            name: this.state.name,
            longDesc: this.state.longDesc,
            shortDesc: this.state.shortDesc,
            price: this.state.price,
            nbBed: this.state.nbBed,
            receptionCapacity: this.state.receptionCapacity
        };

        try {
            const token = await localStorage.getItem("token");
            const response = await axios.put(BASE + '/house/' + this.props.match.params.id, {
                house
            },
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });


            if (response && response.status === 200) {
                history.push("/myaccount");
            }
        } catch (error) {
            console.error("error : ", error);
        }
    }

    seeBookings = async () => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.get(BASE + '/bookingHouse/' + this.props.match.params.id,
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });

            if (response && response.status === 200) {
                console.log(response);
                this.setState({ bookings: response.data, seeBooking: true });
            }
        } catch (error) {
            console.error("error : ", error);
        }
    }

    seeDateUnavailable = async () => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.get(BASE + '/unavailability/' + this.props.match.params.id,
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });

            if (response && response.status === 200) {
                console.log(response);
                this.setState({ unavailableDates: response.data, seeUnavailableDates: true });
            }
        } catch (error) {
            console.error("error : ", error);
        }
    }


    deleteImg = async (id) => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.delete(BASE + '/deleteHouseImg/' + id,
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });

            if (response && response.status === 200) {
                this.setState({ delete: true, messageDelete: "Image supprimé !" });
            }
        } catch (error) {
            console.error("error : ", error);
        }
    }

    deleteEquipment = async (id) => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.delete(BASE + '/deleteEquipment/' + id,
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });

            if (response && response.status === 200) {
                this.setState({ deleteEq: true, messageDeleteEq: "Equipement supprimé !" });
            }
        } catch (error) {
            console.error("error : ", error);
            this.setState({ deleteEq: true, messageDeleteEq: "Erreur!" });
        }
    }

    addImg = async (id) => {
        try {
            const formData = new FormData();
            for (let files of this.state.file) {
                formData.append('file', files);
            }
            const house = {
                id: this.state.house.id
            };

            formData.append('house', JSON.stringify(house));

            const token = await localStorage.getItem("token");
            const response = await axios.post(BASE + "/addImage", formData, {
                headers: { "Content-Type": "multipart/form-data;", 'x-access-token': token }
            });

            if (response && response.status === 200) {
                this.setState({ addImage: true, messageImagesAdd: "Images enregistrées !" });
            }
        } catch (error) {
            this.setState({ addImage: true, messageImagesAdd: "Erreur ! !" });
        }
    }

    async getEquipments() {
        this.setState({ loading: true });
        try {
            const response = await axios.get(BASE + '/equipments');
            if (response && response.status === 200) {
                this.setState({ equipments: response.data, loading: false });
            } else {
                this.setState({ loading: false });
            }
        } catch (error) {
            console.error(error);
            this.setState({ loading: false });
        }
    }

    onTagsChange = (event, values) => {
        this.setState({
            selectedEquipment: values
        }, () => {
            // This will output an array of objects
            // given by Autocompelte options property.
            console.log(this.state.selectedEquipment);
        });
    };

    addEquipments = async (id) => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.post(BASE + '/addEquipment/' + id, { equipments: this.state.selectedEquipment },
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });
            if (response && response.status === 200) {
                this.setState({ deleteEq: true, messageDeleteEq: "Equipement ajouté !" });
            }
        } catch (error) {
            console.error("error : ", error);
            this.setState({ deleteEq: true, messageDeleteEq: "Erreur!" });
        }
    }

    deleteDateUnavailable = async (id) => {
        try {
            const token = await localStorage.getItem("token");
            const response = await axios.delete(BASE + '/unavailability/' + id,
                {
                    headers: {
                        'x-access-token': token,
                        "Content-Type": "application/json"
                    }
                });
            if (response && response.status === 200) {
                this.setState({ deleteDate: true, messageDeleteDate: "Date supprimé !" });
            }
        } catch (error) {
            console.error("error : ", error);
            this.setState({ deleteEq: true, messageDeleteEq: "Erreur!" });
        }
    }

    render() {
        return <div className="ProductDetail" style={{ marginTop: "100px" }}>
            <Helmet>
                <meta charSet="utf-8" />
                <title>{this.state.house.name}</title>
            </Helmet>
            <Container className={useStyles.cardGrid} maxWidth="md" style={{ maxWidth: "1308px" }}>
                {this.state.house && !this.state.loading ?
                    <div>
                        <div style={{ margin: "10px 0", width: "100%" }}>
                            <p>{this.state.house.comments && this.state.house.comments.length > 0 ? <StarRating value={this.getAvererageGradeRate(this.state.house.comments)} readOnly='true' /> : "Aucun commentaire pour le moment"} </p>
                            <div className="container-slide-map" >
                                {this.state.house.houseImgs &&
                                    <div  className="slide-container">
                                        <Slide>
                                            {
                                                this.state.house.houseImgs && this.state.house.houseImgs.map(house =>
                                                    <div style={{
                                                        width: "100%",
                                                        minHeight: "400px",
                                                        backgroundImage: `url("https://imageatypic.s3.us-east-1.amazonaws.com/${house.img}")`,
                                                        backgroundSize: "cover"
                                                    }}>
                                                    </div>
                                                )}
                                        </Slide>
                                    </div>
                                }
                            </div>
                            <div className="text-adress" align="right">
                                <p><RoomIcon />Adresse : {this.state.house.street}, {this.state.house.postalCode} {this.state.house.city}</p>
                            </div>

                            <div>
                                <div style={{ display: "flex", justifyContent: "center", width: "100%" }}>
                                    <h2 style={{ color: "#ab3535" }}>{this.state.house.name}</h2>
                                </div>
                                <div className="container-desc">
                                    <div className="descp-text">
                                        <h3 style={{ color: "#ab3535" }}>Description : </h3>
                                        <p style={{ textAlign: "justify" }}>{this.state.house.shortDesc}</p>
                                        <p style={{ textAlign: "justify" }}>{this.state.house.longDesc}</p>

                                        <p style={{ textAlign: "justify" }}>Nom : </p>

                                        <TextareaAutosize aria-label="minimum height" value={this.state.name} onChange={this.onChangeInput("name")} minRows={8} placeholder="Nom" />
                                        <br />
                                        <br />
                                        <p style={{ textAlign: "justify" }}>Courte description : </p>

                                        <TextareaAutosize aria-label="minimum height" value={this.state.shortDesc} onChange={this.onChangeInput("shortDesc")} minRows={8} placeholder="Courte description" />
                                        <br />
                                        <br />
                                        <p style={{ textAlign: "justify" }}>Longue description : </p>

                                        <TextareaAutosize aria-label="minimum height" value={this.state.longDesc} onChange={this.onChangeInput("longDesc")} minRows={8} placeholder="Longue description" />
                                        <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                                            id="price"
                                            label="Prix"
                                            type="number"
                                            className={"price"}
                                            InputLabelProps={{ shrink: true }}
                                            value={this.state.price}
                                            onChange={this.onChangeInput("price")} />
                                        <br />
                                        <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                                            id="nbBed"
                                            label="Nombre de lit"
                                            type="number"
                                            className={"nbBed"}
                                            InputLabelProps={{ shrink: true }}
                                            value={this.state.nbBed}
                                            onChange={this.onChangeInput("nbBed")} />
                                        <TextField style={{ marginLeft: "10px", marginRigth: "10px" }}
                                            id="receptionCapacity"
                                            label="Capacité"
                                            type="number"
                                            className={"receptionCapacity"}
                                            InputLabelProps={{ shrink: true }}
                                            value={this.state.receptionCapacity}
                                            onChange={this.onChangeInput("receptionCapacity")} />
                                        <div align = "center" style={{marginTop: "2%"}} >
                                        <Button variant="contained" color="primary" onClick={() => this.handleSubmit()}>Enregistrer mes modification</Button>
                                        </div>
                                    </div>
                                    {this.getPopIn()}

                                </div>
                                <div style={{marginTop :"3%"}}>
                                    <Button variant="contained" color="primary" onClick={() => this.seeDateUnavailable()}>Voir les dates d'indisponibilités</Button>
                                    {this.state.deleteDate && <p>{this.state.messageDeleteDate}</p>}
                                    {this.state.seeUnavailableDates && this.state.unavailableDates && this.state.unavailableDates.length > 0 && this.state.unavailableDates.map(unavailableDate =>
                                        <div style={{ "border": "solid", margin: "10px" }}>
                                            <p>ID : {unavailableDate.id}</p>
                                            <p>Date : {this.formatDate(unavailableDate.date)}</p>
                                            <div align ="center">
                                            <Button variant="contained" color="primary" onClick={() => this.deleteDateUnavailable(unavailableDate.id)}>Supprimer</Button>
                                            </div>
                                        </div>)}
                                    <br />
                                </div>
                            </div>
                        </div>

                        <div align="center">
                            <h2>Ajouter des images</h2>

                            <div className="upload-btn-wrapper" style={{marginTop :"0"}}>
                                <button  className="btn">Ajouter vos photo</button>
                                <input  name="myfile" type="file" multiple onChange={event => this.setState({file: event.target.files})}/>
                            </div>
                            <div style={{marginTop :"1%"}}>
                            <Button variant="contained" color="primary" onClick={() => this.addImg(this.state.house.id)}>Ajouter les images</Button>
                            </div>
                            <br />
                            {this.state.addImage && <p>{this.state.messageImagesAdd}</p>}
                        </div>

                        <div style={{ margin: "3%" }}>

                            <h2 align ="center">Supprimer une image</h2>

                            {this.state.house.houseImgs && this.state.house.houseImgs.map(house =>
                                    <div align ="center">
                                        <div style={{ maxWidth: "300px", minWidth :"300px" }} >
                                            <img src={"https://imageatypic.s3.us-east-1.amazonaws.com/" + house.img} alt={house.id} style={{ "width": "100%" }} />
                                        </div>
                                        <div>
                                            <Button variant="contained" color="primary" onClick={() => this.deleteImg(house.id)}>Supprimer cette image</Button>
                                        </div>
                                    </div>


                            )}
                            {this.state.delete && <p>{this.state.messageDelete}</p>}
                        </div>

                        <div align = "center">
                            <h2>Ajouter des équipement</h2>
                            {this.state.equipments && this.state.equipments.length > 0 && <Autocomplete
                                style={{width : "40%"}}
                                multiple
                                id="tags-standard"
                                options={this.state.equipments}
                                getOptionLabel={(option) => option.name}
                                onChange={this.onTagsChange}
                                //getOptionDisabled={option => !!this.state.house.equipment.find(option.name)}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        label="Equipements"
                                        placeholder="Equipements"
                                    />
                                )}
                            />}
                            {this.state.selectedEquipment && this.state.selectedEquipment.length > 0 && <Button variant="contained" color="primary" onClick={() => this.addEquipments(this.state.house.id)}>Ajouter les Equipements</Button>}
                        </div>

                        <div align = "center" >
                            <h2>Supprimer un équipement</h2>
                            {this.state.house.equipment && this.state.house.equipment.map(equipment =>
                                <div>
                                    <p>{equipment.name}</p>
                                    <Button variant="contained" color="primary" onClick={() => this.deleteEquipment(equipment.id)}>Supprimer cet Equipement</Button>
                                </div>
                            )}
                            {this.state.deleteEq && <p>{this.state.messageDeleteEq}</p>}
                        </div>
                        <div align= "center" style={{marginTop : "2%"}}>
                        <Button variant="contained" color="primary" onClick={() => this.seeBookings()}>Voir les bookings</Button>
                        </div >

                        {this.state.seeBooking && this.state.bookings && this.state.bookings.length > 0 && this.state.bookings.map(booking =>
                            <div align= "center" style={{ "border": "solid", margin: "10px" }}>
                                <p>Booking No {booking.id}</p>
                                <p>Date de début : {this.formatDate(booking.startDate)}</p>
                                <p>Date de fin : {this.formatDate(booking.endDate)}</p>
                                <p>Prix de la nuit : {booking.price} €</p>
                            </div>
                        )
                        }
                        <br />
                    </div> : <div>
                        <Spinner />
                    </div>
                }
            </Container>
            {this.state.loading ? "" : <FooterCusto />}
        </div>;
    }

}

export default SeeReservations;
