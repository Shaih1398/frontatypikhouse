import React from "react";
import { CardElement } from "@stripe/react-stripe-js";
import Spinner from "../Component/Spinner";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Button from "@material-ui/core/Button";

const cardStyle = {
	hidePostalCode: true,
	style: {
		base: {
			color: "#32325d",
			fontFamily: "Arial, sans-serif",
			fontSmoothing: "antialiased",
			fontSize: "16px",
			width : "100%",
			"::placeholder": {
				color: "#32325d"
			}
		},
		invalid: {
			color: "#fa755a",
			iconColor: "#fa755a"
		}
	}
};

const BASE = ATYPIC_CONFIG.endpoint_url;

export class CheckoutComponent extends React.Component {

	state = {
		isLoading: false,
		isError: false,
		errorValue: ""
	};


	async componentDidMount() {
		await this.fetchDataStripes();
	}


	async fetchDataStripes() {
		let token = localStorage.getItem("token");
		let { amount, begin, end, idHouse, numPerson } = this.props;

		await fetch(BASE + "/booking/createPayment", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				'x-access-token': token
			},
			body: JSON.stringify({ items: [{ amount: amount, begin: begin, end: end, houseId: idHouse, numPerson: numPerson, cancelResaId: this.props.cancelResaId }] })
		})
			.then(res => {
				return res.json();
			})
			.then(data => {
				this.setState({ clientSecret: data.clientSecret });
			});
	}


	handleSubmit = async (event) => {
		try {

			this.setState({ isLoading: true });
			// Block native form submission.
			event.preventDefault();

			const { stripe, elements } = this.props;

			if (!stripe || !elements) {
				// Stripe.js has not loaded yet. Make sure to disable
				// form submission until Stripe.js has loaded.
				return;
			}

			// Get a reference to a mounted CardElement. Elements knows how
			// to find your CardElement because there can only ever be one of
			// each type of element.
			const cardElement = elements.getElement(CardElement);
			const payload = await stripe.confirmCardPayment(this.state.clientSecret, {
				payment_method: {
					type: "card",
					card: cardElement,
					billing_details: {
						address: {
							line1: "Line test Shai",
							//country: "France",
							city: "la street Shai"
						},
						name: "Shai",
						email: "shaihaddad02@cloudjungle.io",
						phone: "0609758945"
						//address
						//ville
					},
					/*customer: this.props.userData.id,*/
					/*payment_method_details: {
						country: "FR"
					}*/
				}
			});


			if (payload.error) {
				console.log("[error]", payload.error);
				this.setState({ errorValue: payload.error.message });
				this.setState({ isError: true });
				this.setState({ isLoading: false });
			} else {
				if (!payload.paymentIntent || payload.paymentIntent.status !== "succeeded") {
					this.setState({ errorValue: "Erreur Serveur" });
					this.setState({ isError: true });
					this.setState({ isLoading: false });
				} else {
					this.setState({ isLoading: false });
					this.setState({ isError: false });
					this.setState({ errorValue: "PAS DERREUR CA MARCHE" });
					this.props.history.push({ pathname: "/confirmBooking", state: { begin: this.props.begin, end: this.props.end, amount: this.props.amount } });
					//return <Redirect to="/" push={true} />
				}
			}
		} catch (e) {
			this.setState({ errorValue: "Erreur. Completez tous les champs et réessayez" });
			this.setState({ isError: true });
			this.setState({ isLoading: false });
		}
	};


	render() {
		const { stripe } = this.props;
		return (
			<form id="payment-form" onSubmit={this.handleSubmit} style={{ border: "1px dotted", padding: "3%" , width : "100%"}}>
				<CardElement id="card-element" options={cardStyle} />
				{!this.state.isLoading ? <Button type="submit" style={{ marginTop: "3%" }} disabled={!stripe}>Confirmer le payement</Button> : <Spinner />}
				<div>{this.state.isError ? this.state.errorValue : null}</div>
			</form>
		);
	}
}
