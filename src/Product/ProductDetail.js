import React from 'react';
import axios from "axios";
import Container from "@material-ui/core/Container";
import { useStyles } from "../Home/styles";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import { loadStripe } from "@stripe/stripe-js";
import { Elements, ElementsConsumer } from "@stripe/react-stripe-js";
import { ATYPIC_CONFIG } from "../Auth/Config";
import { CheckoutComponent } from "./CheckoutComponent";
import Spinner from "../Component/Spinner";
import { Slide } from 'react-slideshow-image';
import RoomIcon from '@material-ui/icons/Room';
import 'react-slideshow-image/dist/styles.css'
import './slider.css';
import StarRating from '../Component/StarRating'
import BadgeItem from "../Component/Badge";
import FloatingActionButtons from "../Component/FloatingButton";
import FooterCusto from "../Component/FooterCusto";
import './ProductDetail.css';
import PoolIcon from '@material-ui/icons/Pool';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import WhatshotIcon from '@material-ui/icons/Whatshot';
import MapsCusto from '../Component/Maps';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Helmet} from "react-helmet";


const BASE = ATYPIC_CONFIG.endpoint_url;

const promise = loadStripe(ATYPIC_CONFIG.publicKeyStripesTest);

class ProductDetail extends React.Component {

	async componentDidMount() {
		await this.getHouse();
	}

	state = {
		house: [],
		loading: false,
		open: false,
		picker1: "",
		picker2: "",
		validPicker: false,
		textValid: "",
		numberOfNight: 0,
		totalPrice: 0,
		isVisiblePay: false,
		seeComments: false,
		numberComment: 0,
		floatButtonIcon: "+",
		geolocationGoogle: {},
		checkbox: false
	};


	onChangeInput = (field) => {
		return (event) => {
			this.setState({
				[field]: event.target.value,
			});
		};
	};

	getDateFormattingForField() {
		return new Date().toISOString().split('T')[0]
	}


	async getHouse() {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/house/' + this.props.match.params.id);
			if (response && response.status === 200) {
				this.setState({ house: response.data[0], loading: false });
				this.setState({ showcomments: this.state.house.comments.length })
				this.setState({ numberComment: this.state.house.comments.length - 2 })

			} else {
				this.setState({ loading: false });
			}
		} catch (error) {

			this.setState({ loading: false });
		}
	}

	async getAvailability(beginDate, endDate) {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/booking/available/' + this.props.match.params.id + '/' + beginDate + '/' + endDate);
			if (response && response.status === 200) {
				this.setState({ loading: false });
				return response.data;
			} else {
				this.setState({ loading: false });
				return false;
			}
		} catch (error) {

			this.setState({ loading: false });
		}
	}

	getPopIn() {
		return <div className={"c-booking"}>
			<h3 style={{ color: "#ab3535" }}>Vérifier les disponibilités</h3>
			<TextField style={{ width: "100%", }}
				id="picker1"
				label="Arrivée"
				type="date"
				className={"textField"}
				InputLabelProps={{ shrink: true }}
				value={this.state.picker1}
				InputProps={{ inputProps: { min: this.getDateFormattingForField() } }}
				onChange={this.onChangeInput("picker1")} />
			<TextField style={{ width: "100%", marginTop: "20px" }}
				id="picker2"
				label="Départ"
				type="date"
				className={"textField"}
				InputLabelProps={{ shrink: true }}
				value={this.state.picker2}
				disabled={!this.state.picker1}
				InputProps={{ inputProps: { min: this.state.picker1 ? this.state.picker1 : null } }}
				onChange={this.onChangeInput("picker2")} />

			<Button style={{ marginTop: "20px", width: "50%" }} variant="contained" color="primary" onClick={() => this.handleClose()}>Réserver</Button>
			{!this.state.validPicker && this.state.picker1 && this.state.picker2 && this.state.textValid ? <p>{this.state.textValid}</p> : ''}
		</div>
	}

	getNumberOfNight(beginDate, endDate) {
		let date1 = new Date(beginDate);
		let date2 = new Date(endDate);

		// To calculate the time difference of two dates
		let differenceInTimes = date2.getTime() - date1.getTime();

		// To calculate the no. of days between two dates
		return differenceInTimes / (1000 * 3600 * 24);
	}

	onClickFloating = () => {
		if (this.state.numberComment > 0) {
			this.setState({ numberComment: this.state.numberComment - 2 })
		} else {
			this.setState({ numberComment: this.state.house.comments.length - 2 })
		}
	}

	async handleClose() {
		let available;
		if (this.state.picker1 && this.state.picker2) {
			available = await this.getAvailability(this.state.picker1, this.state.picker2);
			if (available && available.canBook) {
				const numberOfNight = this.getNumberOfNight(this.state.picker1, this.state.picker2);
				if (numberOfNight > 0) {
					const totalPrice = this.getTotalPrice(this.state.house.price, numberOfNight);
					this.setState({ validPicker: true, textValid: "Ces dates sont disponibles", numberOfNight: numberOfNight, totalPrice: totalPrice });
				} else {
					this.setState({ validPicker: false, textValid: "Ces dates sont indisponibles", numberOfNight: 0, totalPrice: 0 });
				}
			} else {
				this.setState({ validPicker: false, textValid: "Ces dates sont indisponibles", numberOfNight: 0, totalPrice: 0 });
			}
		} else {
			this.setState({ validPicker: false, textValid: "Vous avez mal selectionnés vos dates", numberOfNight: 0, totalPrice: 0 });
		}

		this.setState({
			open: false
		})
	}

	getTotalPrice(priceByNight, numberOfNight) {
		return priceByNight * numberOfNight;
	}

	getAvererageGradeRate(comments) {
		const mapArray = comments.map(comment => comment.rate);
		return mapArray.reduce((a, b) => a + b) / mapArray.length;
	}

	handleChange = (event) => {
		console.log(event.target.value);
		this.setState({ numPerson: event.target.value });
	};

	render() {
		return <div className="ProductDetail" style={{ marginTop: "100px" }}>
			<Helmet>
				<meta charSet="utf-8" />
				<title>{this.state.house.name}</title>
			</Helmet>
			<Container className={useStyles.cardGrid} maxWidth="md" style={{ maxWidth: "1308px" }}>
				{this.state.house && !this.state.loading ?
					<div>
						<div style={{ margin: "10px 0", width: "100%" }}>

							<p>{this.state.house.comments && this.state.house.comments.length > 0 ? <StarRating value={this.getAvererageGradeRate(this.state.house.comments)} readOnly='true' /> : "Aucun commentaire pour le moment"} </p>
							<div className="container-slide-map" >
								{this.state.house.houseImgs &&
									<div className="slide-container">
										<Slide>
											{
												this.state.house.houseImgs && this.state.house.houseImgs.map(house =>
													<div style={{
														width: "100%",
														minHeight: "400px",
														backgroundImage: `url("https://imageatypic.s3.us-east-1.amazonaws.com/${house.img}")`,
														backgroundSize: "cover"
													}}>
													</div>
												)}
										</Slide>
									</div>
								}
								<div style={{ width: "50%" }}>
									<MapsCusto />
								</div>
							</div>
							<div className="text-adress" align="right">
								<p><RoomIcon />Adresse : {this.state.house.street}, {this.state.house.postalCode} {this.state.house.city}</p>
								<p style={{ textAlign: "right" }}>Propriétaire : {this.state.house.user && this.state.house.user.firstName}</p>
							</div>

							<div>
								<div style={{ display: "flex", justifyContent: "center", width: "100%" }}>
									<h2 style={{ color: "#ab3535" }}>{this.state.house.name}</h2>
								</div>
								<div className="container-desc">
									<div className="descp-text" style={{}}>
										<h3 style={{ color: "#ab3535" }}>Description</h3>
										<p style={{ textAlign: "justify" }}>{this.state.house.shortDesc}</p>
										<p style={{ textAlign: "justify" }}>{this.state.house.longDesc}</p>
									</div>
									{this.getPopIn()}

								</div>
							</div>
						</div>
						<div style={{ display: "flex" }}>
							<div className={"details"}>
								{this.state.numberOfNight > 0 && this.state.validPicker && <div className={"inDetails"}>
									{this.state.numberOfNight > 0 && <p>Nombre total de nuits : {this.state.numberOfNight}<br />
										Nombre de personne : {this.state.numPerson}</p>}
									<p>{this.state.textValid}</p>
									<Grid item xs={12}>
										<FormControlLabel
											control={<Checkbox value="allowExtraEmails" color="primary" checked={this.state.checkbox}
												onChange={()=> this.setState({checkbox : !this.state.checkbox})} />}
											label="J'accepte les conditions générales de vente"
										/>
									</Grid>
									{this.state.validPicker && this.state.numberOfNight > 0 && this.state.totalPrice > 0 &&
										<div style={{ width: "100%" }}><p>Prix Total : {this.state.totalPrice} €</p>
											{this.state.checkbox && <Button variant="contained" color="primary" onClick={() => this.setState({
												isVisiblePay: true
											})}>Payer</Button>}
										</div>
									}
								</div>}
							</div>
							{this.state.isVisiblePay && <Elements stripe={promise}>
								<ElementsConsumer>{({ elements, stripe }) => (
									<CheckoutComponent elements={elements} stripe={stripe} amount={this.state.totalPrice} idHouse={this.state.house.id} history={this.props.history} numPerson={this.state.numPerson}
										begin={this.state.picker1} end={this.state.picker2} />
								)}
								</ElementsConsumer>
							</Elements>}
						</div>

						<div className="container-equip">
							<div>
								<div className="equi-text">
									<h3 style={{ color: "#ab3535" }} align="center">Equipements</h3>
									<div>
										{this.state.house.equipment && this.state.house.equipment.length > 0 &&

											<div style={{ display: "flex", justifyContent: "space-around", flexWrap: "wrap" }} className={""}> {this.state.house.equipment.map(equipment => <div>
												<div style={{color : "#ab3535"}}><b>{equipment.name} </b>  </div>
											</div>)}
											</div>}
									</div>
								</div>
							</div>
						</div>

						<div className="equi-text">
							<h3 style={{ color: "#ab3535" }} align="center">Propriété dynamiques</h3>
							<div>
								{this.state.house.dynamicPropertiesHouses && this.state.house.dynamicPropertiesHouses.length > 0 &&
								<div style={{ display: "flex", justifyContent: "space-around", flexWrap: "wrap" }} className={""}> {this.state.house.dynamicPropertiesHouses.map(properties => <div>
									{properties.value && <p>{properties.value}</p>}
								</div>)}
								</div>}
							</div>
						</div>


						{this.state.house.comments && this.state.house.comments.length > 0 && <div style={{ marginTop: "10%" }}>
							<div><BadgeItem content={this.getAvererageGradeRate(this.state.house.comments)} ></BadgeItem><span style={{ marginLeft: "0.8rem" }}>{this.state.house.comments.length} </span> <span style={{ fontSize: "1em" }}> Commentaire(s) </span> </div>

							<div className={"c-flex-wrap"}>
								{this.state.house.comments.slice(0, this.state.house.comments.length - this.state.numberComment).map(comment =>
									<div style={{width: "50%",}}>
										<div>Auteur :</div>
										<div>
											<StarRating value={comment.rate} readOnly></StarRating> {comment.comment} </div>
									</div>)}

								<div className={"c-flex"} style={{ justifyContent: "center", width: "100%" }}>
									<FloatingActionButtons onChange={() => this.onClickFloating()} >{this.state.floatButtonIcon}</FloatingActionButtons>
								</div>
							</div>
						</div>}
						<br />
					</div> : <div>
						<Spinner />
					</div>
				}
			</Container>
			{this.state.loading ? "" : <FooterCusto />}
		</div>;
	}
}

export default ProductDetail;
