import jwtDecode from "jwt-decode";
/*
Toutes ces fonctions ne sont pas testées pour le moment
*/

export function checkTokenExpirationMiddleware() {
	const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;
	if (token && jwtDecode(token).exp < Date.now() / 1000) {
		localStorage.clear();
		return false;
	}
	return true;
}

export function logoutUser() {
	localStorage.clear();

	window.location.replace("/")
}


export function checkTokenExpirationMiddlewareAndStatus() {
	const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;
	if (token && jwtDecode(token).exp < Date.now() / 1000) {
		localStorage.clear();
		return false;
	}
	return jwtDecode(token).status === 1;
}



export function getStatus(){
	const token = localStorage.getItem("token") ? localStorage.getItem("token") : null;
	return jwtDecode(token).status === 1;
}
