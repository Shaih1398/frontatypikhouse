import React from 'react';
import axios from "axios";
import {ATYPIC_CONFIG} from "../Auth/Config";
import Alert from '@material-ui/lab/Alert';
import Spinner from "../Component/Spinner";

const BASE = ATYPIC_CONFIG.endpoint_url;

class confirmEmail extends React.Component {

	state = {
		loading: false,
		error: true
	};

	async componentDidMount() {
		await this.verifyEmail()
	}

	async verifyEmail() {
		this.setState({loading: true});
		try {
			const response = await axios.get(BASE + '/activate/' + this.props.match.params.token);

			if (response) {
				this.setState({error: false, loading: false});
			}
		} catch (error) {
			console.error(error);
			this.setState({error: true, loading: false});
		}
	}

	render() {
		return !this.state.loading ? this.state.error ?
			<Alert severity="error" style={{margin: "40px auto", width: "50%"}}>
				Impossible de valider votre compte
			</Alert> :
			<Alert severity="success" style={{margin: "40px auto", width: "50%"}}>
				Votre compte est bien validé ! Cliquez
				<a style={{textDecorationLine: "none"}} href={'https://dsp-dev019-sh-ri-de.fr/signIn'}>ici</a> pour vous connectez</Alert> : <Spinner/>
	}
}

export default confirmEmail;
