import React, {useState} from 'react';
import axios from 'axios';
import {ATYPIC_CONFIG} from "../Auth/Config";

const FileUpload = () => {
	// If you are using TypeScript the state will be
	// const [file, setFile] = useState<FileList | null>(null);
	const [file, setFile] = useState(null);
	const BASE = ATYPIC_CONFIG.endpoint_url;

	const submitFile = async () => {
		try {
			if (!file) {
				throw new Error('Select a file first!');
			}


			const formData = new FormData();
			for(let files of file){
				formData.append('file', files);
			}
			//formData.append('file', file[0]);
			await axios.post(BASE + "/upload/files", formData, {
				headers: {
					'Content-Type': 'multipart/form-data',
				},
			});
			// handle success
		} catch (error) {
			// handle error
		}
	};

	return (
		<div>
			<label>Upload file</label>
			<input type="file" multiple onChange={event => setFile(event.target.files)}/>
			<button onClick={() => submitFile()} type="submit">Send</button>
		</div>
	);
};

export default FileUpload;
