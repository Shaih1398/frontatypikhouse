import React from 'react';
import axios from "axios";
import {ATYPIC_CONFIG} from "../Auth/Config";
import Spinner from "../Component/Spinner";

const BASE = ATYPIC_CONFIG.endpoint_url;

class Categories extends React.Component {

	async componentDidMount() {
		await this.getCategories();
	}

	state = {
		categories: [],
		loading: false
	};

	async getCategories() {
		this.setState({loading: true});
		try {
			const response = await axios.get(BASE + '/category');
			if (response && response.status === 200) {
				this.setState({categories: response.data, loading: false});
			} else {
				this.setState({loading: false});
			}
		} catch (error) {
			console.error(error);
			this.setState({loading: false});
		}
	}


	render() {
		return <div className="Categories">
			<p>Voici nos categories </p>
			{this.state.categories && this.state.categories.length > 0 && !this.state.loading ?
				this.state.categories.map(categories => <p>{categories.name}</p>)
				: <div><Spinner/></div>}
		</div>
	}
}

export default Categories;
