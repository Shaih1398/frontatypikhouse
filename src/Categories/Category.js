import React from 'react';
import axios from "axios";
import { Link } from "react-router-dom";
import Container from "@material-ui/core/Container";
import { useStyles } from "../Home/styles";
import { ATYPIC_CONFIG } from "../Auth/Config";
import Spinner from "../Component/Spinner";
import './Category.css';
import RoomIcon from '@material-ui/icons/Room';
import StarRating from "../Component/StarRating";
import KingBedIcon from '@material-ui/icons/KingBed';
import BathtubIcon from '@material-ui/icons/Bathtub';
import FooterCusto from "../Component/FooterCusto";
import {Helmet} from "react-helmet";



const BASE = ATYPIC_CONFIG.endpoint_url;

class Category extends React.Component {

	async componentDidMount() {
		await this.getCategories();
	}

	state = {
		categories: [],
		loading: false
	};

	async getCategories() {
		this.setState({ loading: true });
		try {
			const response = await axios.get(BASE + '/categoryHouse/' + this.props.match.params.id);
			if (response && response.status === 200) {
				this.setState({ categories: response.data, loading: false });
			} else {
				this.setState({ loading: false });
			}
		} catch (error) {
			console.error(error);
			this.setState({ loading: false });
		}
	}

	getAvererageGradeRate(comments) {
		const mapArray = comments.map(comment => comment.rate);
		return mapArray.reduce((a, b) => a + b) / mapArray.length;
	}

	render() {
		return <>
			<Helmet>
				<meta charSet="utf-8" />
				<title>Catégorie</title>
			</Helmet>
			<main style={{ backgroundColor: "#F7F7F7" }}>
				<Container style={{ marginTop: "5%" }} className={useStyles.cardGrid} >
					<p>Toutes houses de la category </p>
					<div style={{width :"100%"}} className="c-flex-wrap-category" >
						{this.state.categories && this.state.categories.length > 0 && !this.state.loading ?
							this.state.categories.map(
								house => !house.isBlock &&
									<div key={house.id} className="c-card-cat" style={{ width: "33%", marginBottom: "3%", backgroundColor: "white", borderRadius: "5px", position: "relative", overflow: "hidden" }}>
										<div style={{ height: "175px", overflow: "hidden", clear: "both" }}>
											{house && house.houseImgs.length > 0 && house.houseImgs[0].img && <img style={{ width: "100%", height: "100%", borderTopLeftRadius: "3%", borderTopRightRadius: "3%" }} src={"https://imageatypic.s3.us-east-1.amazonaws.com/" + house.houseImgs[0].img} className={"imgCategory"} alt={house.houseImgs[0].img} />}
										</div>
										<div style={{ minHeight: "120px" }}>
											<div style={{
												fontSize: "0.8rem", minHeight: "50px",
												color: "#9fa7a7", fontWeight: "bold", border: "1px solid #E4E4E4", padding: "0px 20px 20px"
											}}>
												<div style={{ minHeight: "65px" }}>
													<Link to={"/product/" + house.id}><h3 className="house-title" style={{ fontSize: "18px", fontWeight: "800", color: "black" }}>{house.name} </h3> </Link> </div>
												<div>
													<div style={{ display: "flex" }}>
														<div><RoomIcon style={{ width: "0.6em" }} /></div>
														<div style={{ marginTop: "0px" }}><span style={{ fontSize: "12px", color: "#aeb4b6" }}>{house.city}</span></div>
													</div>

												</div>
												<div>
													{house.comments && house.comments.length > 0 ? <StarRating value={this.getAvererageGradeRate(house.comments)} readOnly={true} /> : <StarRating value="0" readOnly="true"></StarRating>}

												</div>
											</div>
										</div>
										<div style={{ padding: "20px 20px 20px", border: "1px solid #E4E4E4", borderTop: "none" }}>
											<div style={{ justifyContent: "space-around" }} className="c-flex-wrap-category">
												<div className="c-flex" >
													<KingBedIcon style={{ color: "black" }} ></KingBedIcon>
												</div>
												<div>
													<span style={{ fontSize: "13px", color: "black" }}> {house.nbBed} Chambre(s)</span>
												</div>
												<div className="c-flex" >
													<BathtubIcon style={{ color: "black" }}></BathtubIcon>
												</div>
												<div>
													<span style={{ fontSize: "13px", color: "black" }}> {house.nbBed} Salle de bain(s)</span>
												</div>
											</div>
											<div style={{ display: "flex" }}>
												<div style={{ marginRight: "31%", marginTop: "20px" }}><span style={{ fontSize: "20px", color: "black", fontWeight: "bold" }}>{house.price}€/nuit </span></div>
												<div style={{ marginTop: "20px" }}>
													<Link to={"/product/" + house.id}>  <span style={{ color: "#61B345", fontSize: "13px" }}>Voir le logement </span> </Link>
												</div>
											</div>
										</div>
									</div>
							) : <div><Spinner /></div>}
					</div>

				</Container>

			</main>
			{this.state.loading ? "" : <FooterCusto />}

		</>
	}
}

export default Category;

