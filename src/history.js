let history;
if (typeof window !== 'undefined') {

	const createBrowserHistory = require('history/createBrowserHistory').default;

	history = createBrowserHistory();
}

export default history
